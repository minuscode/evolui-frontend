<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>
 

  </head>
  <body> 

  <?php include 'include.php';?>

    <section class="regulamento-page">
      
    
      <!-- HEADER -->

      <header class="header">
        <?php echo $menu;?>
      </header>

      

      <div class="wrapper">
        <main class="main">
  

        <section class="course-holder">

         <hgroup>
           <h1>Regulamento de Formação e Condições Gerais de Utilização</h1>
           <p>
             Os presentes termos e condições de serviço regulam as relações entre o cliente do serviço EVOLUI.COM® pessoa física e individual adiante designado de cliente, e a entidade que fornece os serviços, Cação e Melo, Consultores de Gestão Lda., adiante designada de EVOLUI.COM®, marca registada que detém. A utilização dos serviços EVOLUI.COM® implica a aceitação e concordância com os termos e condições de serviço por parte do Cliente, que consubstanciam um contrato de prestação de serviços de formação e um regulamento de formação.
           </p>
         </hgroup>

         <hgroup>
           <h1>1. Utilização única e individual</h1>
           <p>1.1. O registo como Membro no EVOLUI.COM® e a frequência de cursos implica a aceitação do regulamento de formação e condições gerais de utilização. Ao Cliente é dada a possibilidade de consultar, ler com atenção e decidir se aceita ou não o regulamento e as condições gerais de utilização, sendo vedado o registo e a frequência de cursos a Clientes que não concordem com os termos do regulamento e das condições gerais de utilização. Com a aceitação destes termos, é registado o dia, hora e IP do Cliente, como forma de confirmar a sua aceitação.</p>
           <p>1.2. O registo como Membro no EVOLUI.COM® e qualquer inscrição relacionada com o registo é individual e intransmissível e apenas pode ser usufruída pelo cliente. O Nome de Utilizador (Login) e Código de Acesso (Password) correspondentes a um Membro (registo/ficha de formando) apenas podem ser utilizados pelo Cliente e não podem ser cedidas ou utilizados por terceiros, incluindo entidades que tenham financiado a inscrição do Cliente.</p>
           <p>1.3. Todas as mensagens enviadas para o endereço de e- mail constante no registo do Cliente destinam-se única e exclusivamente ao Cliente e não podem ser copiadas, reproduzidas, dadas a consultar, dadas a utilizar ou de qualquer outra forma dadas a usufruir a outros indivíduos ou entidades.</p>
           <p>1.4. Todos os textos, imagens, serviços interactivos e demais serviços disponibilizados pelo EVOLUI.COM® ao destinam-se única e exclusivamente ao usufruto do Cliente e não podem ser dados a usufruir a qualquer outro indivíduo ou entidade, mesmo que tenham assumido o pagamento dos custos de frequência do curso.</p>
           <p>1.5. Não obstante a utilização ser individual e intransmissível, o cliente pode solicitar a facturação dos serviços a terceiros, q  ue assumam o pagamento das inscrições.</p>
           <p>1.6. O Cliente reconhece que todos os seus acessos e movimentos dentro do site do EVOLUI.COM® são registados e que os mesmos podem ser usados para efeitos legais.</p>
         </hgroup>

         <hgroup>
           <h1>2. Dados do registo de Cliente</h1>
           <p>2.1. Ao efectuar um registo no EVOLUI.COM® (situação também conhecida por "Tornar-se membro do EVOLUI.COM®) o Cliente deverá preencher os seus dados no formulário de inscrição com informação verdadeira e declara estar em situação legal para preencher os referidos dados.</p>
           <p>2.2. O Cliente declara, através da aceitação do presente Regulamento de Formação e Condições Gerais de Utilização, e sob compromisso de honra, que os dados por si preenchidos ao efectuar um registo no EVOLUI.COM® constituem informação verdadeira e relativa apenas e unicamente à sua própria pessoa.</p>
           <p>2.3. Os dados pessoais fornecidos pelo Cliente no formulário de registo e/ou comunicados ao EVOLUI.COM® estão protegidos pela Lei nº 67/98 de 26 de Outubro e são os estritamente necessários para identificar o Cliente e constarão de uma base de dados que será utilizada pelo EVOLUI.COM® de tal forma a que seja possível manter em funcionamento o EVOLUI.COM® e/ou prestar serviços ao Cliente.</p>
           <p>2.4. O Cliente tem direito a consultar e alterar os dados relativos à sua inscrição bastando para isso que contacte, por escrito, o EVOLUI.COM®. O EVOLUI.COM® envidará todos os esforços razoáveis para que seja possível ao Cliente alterar os seus dados através do próprio web site EVOLUI.COM® mediante digitação do Nome do Utilizador e Código de Acesso, sem que no entanto se comprometa a disponibilizar tal forma de alteração a qualquer momento.</p>
           <p>2.5. O tratamento destes dados é da responsabilidade do EVOLUI.COM® que garante a sua confidencialidade e segurança e se obriga a não os ceder a terceiros.</p>
         </hgroup>

         <hgroup>
           <h1>3. Utilização reservada de textos, imagens e serviços</h1>
           <p>3.1. Os textos, imagens, sons, serviços interactivos e demais materiais ou serviços disponibilizados pelo EVOLUI.COM®, adiante designados por conteúdos, destinam-se a ser utilizados para o fim exclusivo de frequência de um curso do EVOLUI.COM®.</p>
           <p>3.2. O cliente pode gravar e imprimir os conteúdos que, no âmbito da formação que estiver a frequentar, lhe forem disponibilizados mas apenas para efeitos da sua aprendizagem.</p>
           <p>3.3. O Cliente não pode ceder os conteúdos a terceiros, copiá-los, reproduzi-los ou manipulá-los no todo ou em parte nem utilizar os conteúdos que lhe são colocados à disposição para fins comerciais, publicitários, de auto-promoção ou formação de terceiros, mesmo que mencione a fonte. O Cliente reconhece ainda que não pode ceder os conteúdos de formação inclusivamente à quaisquer entidades que lhe tenham pago a inscrição no curso que está a frequentar ou frequentou.</p>
           <p>3.4. O Cliente não pode igualmente utilizar o logótipo do EVOLUI.COM® em qualquer circunstância, ainda que seja no âmbito de trabalhos realizados nos cursos que frequentar no EVOLUI.COM®.</p>
           <p>3.5. No caso de publicar online trabalhos realizados no âmbito dos cursos que frequentar no EVOLUI.COM®, e caso pretenda utilizar o nome do EVOLUI.COM®, o Cliente deverá mencionar "este trabalho foi realizado pelo formando (nome do Cliente) no âmbito do curso (nome do curso) do EVOLUI.COM® e é da inteira responsabilidade do seu autor", assumindo assim toda a responsabilidade pelo seu conteúdo e divulgação e isentando o EVOLUI.COM® de qualquer responsabilidade.</p>
           <p>3.6. Todos os conteúdos disponibilizados no ou pelo EVOLUI.COM® são propriedade do EVOLUI.COM® e/ou entidades ou indivíduos parceiros do EVOLUI.COM® e não podem ser copiados, reproduzidos, citados ou manipulados, no todo ou em parte.</p>
           <p>3.7. Os recursos didácticos, conselhos, opiniões, textos, imagens que não tenham o logótipo do EVOLUI.COM® e demais informação disponibilizada é da responsabilidade dos autores, sejam eles formadores ou das entidades parceiras que os desenvolvem, e que estão expressamente indicadas na página de descrição de cada curso, não veiculando qualquer posição oficial ou responsabilidade do EVOLUI.COM®.</p>
         </hgroup>


         <hgroup>
           <h1>4. Regras de conduta</h1>
           <p>4.1. O Cliente aceita reger-se por regras e condutas de boa educação conforme os usos e costumes de Portugal e obriga-se a dirigir- se e interagir com os colaboradores, formadores e restantes clientes do EVOLUI.COM® de forma correcta e educada.</p>
           <p>4.2. O EVOLUI.COM® reserva o direito de editar ou remover quaisquer mensagens colocadas pelo cliente no fórum de discussão dos cursos, caso estejam a provocar distúrbios ao bom funcionamento do curso, ou sejam ofensivas ou alheias às matérias dos cursos e reserva-se o direito de suspender a utilização do serviço por parte do cliente caso essa prática se repita ou seja considerada grave.</p>
           <p>4.3. O Cliente reconhece que fica impedido de:</p>
           <ul>
             <li><p>Colocar nos fóruns de discussão, ou noutros instrumentos de interacção social utilizados na formação, mensagens comerciais não solicitadas (spam), mensagens não relacionadas com as temáticas do curso, reclamações, pedidos de ordem pessoal ou mensagens que perturbem as actividades pedagógicas e o bom ambiente formativo;</p></li>
             <li><p>Recolher informações sobre os Clientes do serviço incluindo, mas não limitado a, nome, telefone e endereço de e-mail, sem prévia autorização dos Clientes visados e do EVOLUI.COM®;</p></li>
             <li><p>Criar uma identidade falsa, fazer-se passar por outra pessoa ou tentar enganar os outros clientes, os formadores ou funcionários do EVOLUI.COM®;</p></li>
             <li><p>Transmitir qualquer material que possa esta protegido por patentes, direitos de autor ou outras formas de protecção de propriedade intelectual e para o qual não tenha direitos de transmissão ou utilização;</p></li>
             <li><p>Transmitir vírus ou qualquer outro tipo de código destrutivo, nefasto que possa causar danos a terceiros.</p></li>
           </ul>
         </hgroup>


         <hgroup>
           <h1>5. Requisitos de utilização</h1>
           <p>5.1. A utilização do serviço EVOLUI.COM® apenas é possível se o Cliente dispuser de acesso a um computador com ligação à Internet (preferencialmente de banda larga, como o ADSL) e um browser (programa para navegar na web), como o Internet Explorer, Firefox, Google Chrome ou Apple Safari, devidamente actualizados nos últimos 6 meses. É ainda necessário que o Cliente tenha competências actualizadas no domínio das novas tecnologias de informação e comunicação (TIC) e seja maior de idade. A frequência de cursos de formação implica igualmente a aceitação do Regulamento de Formação e das Condições Gerais de Utilização.</p>
           <p>5.2. O cliente compromete-se ainda a, antes de proceder à inscrição, garantir o cumprimento dos requisitos adicionais e necessários à frequência de um determinado curso, nomeadamente ao nível do equipamento, software, competências, experiência profissional, habilitações académicas e domínio de línguas estrangeiras entre outros que sejam mencionados na página de descrição do curso que pretende frequentar e entregar todos os documentos comprovativos desse cumprimento.</p>
         </hgroup>
         

        </section> 


      </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>
      </footer>


    </section>
  </body>
</html>







