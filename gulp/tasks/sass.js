var gulp = require('gulp');
var sass = require('gulp-ruby-sass');

var path = require('path');

var sassFiles = {
  src: path.resolve('src/sass', 'style.scss'),
  dst: path.resolve('./assets/css')
};

gulp.task('sass', function () {
  gulp.src(sassFiles.src)
    .pipe(sass())
    .on('error', function (err) { console.log(err.message); })
    .pipe(gulp.dest(sassFiles.dst));
});
