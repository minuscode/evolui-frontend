var gulp = require('gulp');

// Plugins
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

var path = require('path');

var js = {
  src: [
    './src/vendor/jquery/dist/jquery.min.js',
	'./src/vendor/masonry/dist/masonry.pkgd.js'
  ],
  dst: path.resolve('assets', 'js')
};

gulp.task('js', function () {
  return gulp.src(js.src)
    .pipe(sourcemaps.init())
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(js.dst));
});
