var gulp = require('gulp');

var watch = {
  js:   ['src/js/**/*.js'],
  html: ['**/*.html'],
  sass: ['src/sass/**'],
  img:  ['src/img/**']
};

gulp.task('watch', function () {
  gulp.watch(watch.js, ['js']);
  gulp.watch(watch.sass, ['sass']);
  gulp.watch(watch.img, ['images']);
});
