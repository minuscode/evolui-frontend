var gulp = require('gulp');

var path = require('path');

var fonts = {
  src: path.resolve('src/fonts/', '*'),
  dst: path.resolve('assets/fonts')
};


gulp.task('fonts', function() {
  return gulp.src(fonts.src)
    .pipe(gulp.dest(fonts.dst));
});