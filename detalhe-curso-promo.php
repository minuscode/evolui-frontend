<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="details-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>
      <?php echo $search;?>

      

      <div class="wrapper">
        <div class="banner">
        
      </div>


      <main class="main">


        <section class="course-details">

          <img src="assets/img/formadores/promo.png" alt="Promoção" class="promo-course">


          <article class="course-header">
            
            <div class="course-title">
              <h1>A Actividade de Transporte e a Logística</h1>  
            </div>

            <div class="course-favorite">
              <a href="#" class="btn-orange">Inscreva-se neste curso</a>
              <a href="#" class="add-favorite"><span class="icon icon-star"></span>Marcar como favorito</a>
            </div>

          </article>
        

          <article class="course-detail--holder">

            

            <div class="course-detail--holder_box">
              <h2>Resumo</h2>
              <p>
                Os transportes e a logística são parte integrante nas nossas vidas e assumem uma tal importância que se torna imperioso conhecer como se desenvolvem todas as actividades em torno destas duas áreas.
              </p>
            </div>

            <div class="course-detail--holder_box">
              <h2>Objectivo</h2>
              <p>Conhecer como se desenvolve a actividade de transporte e a logística.</p>
            </div>

            <div class="course-detail--holder_box">
              <h2>Competências a desenvolver</h2>
              
              <p>Entender a importância do transporte</p>
              <p>Definir transporte</p>
              <p>Descrever de um modo sucinto a evolução de cada modo de transporte</p>
              <p>Conhecer alguns conceitos directamente ligados ao transporte e à logística</p>
              <p>Identificar as principais características dos mais importantes modos de transporte</p>
              <p>Conhecer as mais relevantes vantagens e desvantagens dos principais modos de transporte</p>
              <p>Efectuar a comparação entre os principais modos de transporte</p>
              <p>Avaliar a intercomplementariedade entre os principais modos de transporte</p>
              <p>Identificar as principais infra-estruturas dos vários modos de transporte</p>
              <p>Fazer uma análise mais profunda de toda a operação de transporte rodoviário</p>
              <p>Descrever as principais formas e tipos de expedição</p>
              <p>Identificar os principais tipos de embalagens</p>
              <p>Definir logística</p>
              <p>Descrever de um modo sucinto a evolução da logística</p>
              <p>Conhecer as principais características da logística</p>
              <p>Descrever as principais actividades da logística</p>
              <p>Reconhecer a importância da logística na distribuição</p>
              <p>Descrever alguns dos sistemas próprios que cada empresa deverá criar para o seu bom funcionamento</p>
              <p>Caracterizar a gestão de fluxos</p>
              <p>Caracterizar o JIT</p>
              <p>Definir benchmarking logístico</p>
              <p>Desenhar uma cadeia de abastecimento</p>
              <p>Identificar as vantagens do outsourcing logístico</p>
              <p>Avaliar uma estratégia logística</p>
              <p>Caracterizar o serviço prestado ao cliente</p>

            </div>


            <div class="course-detail--holder_box">
              <h2>Destinatários</h2>
              <p>
                Este curso destina-se a todas as pessoas com interesse na temática, pessoas que trabalhem directa ou indirectamente com transportes e logística, motoristas de pesados de mercadorias e de passageiros em preparação para a obtenção do CAP (já obrigatório na U.E. e em iminente transposição para a legislação portuguesa através da directiva 2003/59/CE).
              </p>
            </div>

            <div class="course-detail--holder_box">
              <h2>Programa</h2>
                            
              <p>1. Definição de Transporte</p>
              <p>2. Evolução dos Diferentes Modos de Transporte</p>
              <p>3. Alguns Conceitos Básicos Directamente Ligados aos Transportes e à Logística</p>
              <p>4. As Principais Características, Vantagens e Desvantagens dos Diferentes Modos de Transporte</p>
              <p>5. Comparação entre Modos de Transporte nas Variantes mais Importantes</p>
              <p>6. A Intercomplementariedade entre os Modos de Transporte</p>
              <p>7. As Principais Infra-estruturas dos Diferentes Modos de Transporte</p>
              <p>8. O Transporte Rodoviário de Mercadorias – Abordagem Mais Completa</p>
              <p>9. A Expedição de Mercadorias e os Principais Tipos de Embalagens Utilizados</p>
              <p>10. A Definição de Logística</p>
              <p>11. A Ligação da Evolução da Logística com o que esta Actividade Representa Actualmente</p>
              <p>12. A Distribuição e a Interligação com os Transportes e os Sistemas Próprios de Cada Empresa</p>
              <p>13. O JIT - Just in Time</p>
              <p>14. A Logística Inversa ou Eco-Logística</p>
              <p>15. Benchmarking Logístico, Cadeia de Abastecimento, Outsourcing Logístico, Estratégia Logística e o Sempre Importante Serviço ao Cliente</p>

            </div>

            <div class="course-detail--holder_box">
              <h2>Funcionamento</h2>
              <p>Este curso tem apoio do formador, que esclarecerá as suas dúvidas através de um fórum de discussão, onde poderá colocar as suas questões a qualquer momento durante a duração do curso.</p>
              <p>O curso encontra-se estruturado em aulas, que estão num formato cumpridor dos standards internacionais de e-learning.</p>
              <p>Cada aula tem uma duração média estimada de 2 horas.</p>
              <p>Pode aceder às aulas a qualquer hora do dia ou da noite, bem como fins-de-semana e feriados, em função da sua disponibilidade e disposição, tendo apenas a restrição de ter de terminar o curso até ao último dia de formação.</p>
              <p>As aulas são disponibilizadas na sua Área de Membro de 2 em 2 dias, podendo frequentar cada aula desde o dia em que é disponibilizada até ao último dia de formação. O curso só termina 5 dias depois de ser disponibilizada a última aula.</p>
              <p>Pode ainda interromper uma aula a qualquer momento e recomeçá-la mais tarde, podendo frequentar cada aula quantas vezes desejar.</p>
              <p>No final de cada aula tem a possibilidade de efectuar o download de alguns materiais de formação para gravar no seu computador ou imprimir.</p>
              <p>No final do curso receberá um Certificado de Formação Profissional caso frequente pelo menos 80% das aulas, realize os trabalhos e os testes propostos, participe nas discussões online e tenha avaliação final positiva.</p>
            </div>

            <div class="course-detail--holder_box">
              <h2>Requisitos</h2>
              <p>Para frequentar este curso é necessário ter acesso a um computador ou um tablet com ligação à Internet e um browser (programa para navegar na web), como o Chrome, Safari, Firefox ou Internet Explorer.</p>
              <p>Pode aceder ao curso a partir de qualquer computador (por exemplo, em casa e no escritório), tablet ou smartphone. </p>
              <p>Não necessita de instalar quaisquer programas no seu computador para poder aceder ao curso.</p>
              <p>Este curso não exige qualquer conhecimento prévio do tema.</p>
            </div> 

            <a href="#" class="btn-orange">Inscreva-se neste curso</a>

          </article>


          <!-- ASIDE -->

          <aside class="course-detail--aside">
            
            <div class="course-detail--aside_box">

              <h2>Detalhes do curso</h2>
              <ul class="clearfix">
                <li>
                  <p> <span class="icon icon-price"></span><span class="promo-course--price">€74,90</span> €65,00</p>
                </li>
                <li>
                  <p><span class="icon icon-date"></span> de 18 de Janeiro a 8 de Fevereiro de 2015</p>
                </li>
                <li>
                  <p><span class="icon icon-lessons"></span>8 aulas</p>
                </li>
                <li>
                  <p><span class="icon icon-time"></span> 16 horas</p>
                </li>
              </ul>
            </div>


            <div class="course-detail--aside_box">

              <h2>Formador</h2>
              <ul class="clearfix photo-holder">
                <li>
                  <img class="photo-forma" src="assets/img/formadores/ana-rita-xavier.png" alt="Ana Rita Xavier">
                  <p>Ana Rita Xavier XavierXavier</p>
                </li>
              </ul>
            </div>

            <div class="course-detail--aside_box">

              <h2>Próximas Datas</h2>
              <ul class="clearfix">
                <li>
                  <p>- de 18 de Janeiro a 8 de Fevereiro de 2015</p>
                  <p>- de 10 de Fevereiro a 3 de Março de 2015</p>
                  <p>- de 2 a 23 de Março de 2015</p>
                  <p>- de 22 de Março a 12 de Abril de 2015</p>
                </li>
              </ul>
            </div>

            <div class="course-detail--aside_box course-sugestion--holder">

              <h2>Próximas Datas</h2>
              <ul class="clearfix">
                <li>
                  <img src="assets/img/formadores/course-sugestion.jpg" alt="">
                  <img src="assets/img/formadores/course-sugestion-2.jpg" alt="">
                </li>
              </ul>
            </div>

  
          </aside>

        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







