<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="pay-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>
      <?php echo $search;?>


      <div class="wrapper">
        <main class="main">


        <section class="pay-details--holder">


          <article class="pay-progress">
            
            <ul class="pay-progress--holder">
              <li>
                <div class="circle">
                  <span class="icon icon-check"></span>  
                </div>
                <p class="desc">Detalhes de Compra</p>
              </li>
              <li class="active">
                <div class="circle">
                  <span>2</span>  
                </div>
                <p class="desc">Confirmação</p>
              </li>
              <li>
                <div class="circle">
                  <span>3</span>  
                </div>
                <p class="desc">Resumo</p>
              </li>
            </ul>

          </article>
        
 
          <article class="pay-holder">

            <div class="pay-holder--box">
              <table class="pay-table">
                <thead>
                  <tr>
                    <th>
                      <div>
                        <h2>Descrição</h2>
                      </div>
                    </th>
                    <th>
                      <div>
                        <h2>Preço</h2>
                      </div>
                    </th>
                  </tr>  
                </thead>
                
                <tbody>
                  <tr>
                    <td>
                      <div>
                        <h3>Implementação de Sistemas de Qualidade e Satisfação na Formação  (8h)</h3>
                        <p>Início: 1 de Fevereiro</p>
                        <p>Fim: 20 de Fevereiro</p>
                      </div>
                    </td>

                    <td>
                      <div>
                        <h3>€94,90</h3>
                      </div>
                    </td>
                  </tr>  
                  <tr>
                    <td colspan="2">
                      <div class="total">
                        <h3>Total</h3>
                        <h3>€94,90</h3>
                      </div>
                    </td>
                  </tr>
                </tbody>

              </table>
            </div>

          </article>



          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Dados de Facturação</h2>
              
              <article class="data-box">

                <ul class="dados-fac--confirm">
                  <li>
                    <h2>Maria Luísa dos Santos </h2>
                  </li>
                  <li>
                    <h2>Contribuinte nº 11190387 <span>Portugal</span></h2>
                  </li>
                  <li>
                    <h2>Rua do Alecrim, 29, 2º dto <span>1200-371</span><span>Lisboa</span> </h2>
                  </li>
                </ul>

              </article>

            </div>

          </article>



          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Método de pagamento</h2>
              
              <article class="data-box">

                <ul class="dados-fac--confirm">
                  <li>
                    <h2>Multibanco</h2>
                  </li>  
                </ul>

              </article>

            </div>

          </article> 


          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Descontos</h2>
              
              <article class="data-box">

                <ul class="dados-fac--confirm">
                  <li>
                    <h2>Não possui descontos a aplicar</h2>
                  </li>  
                </ul>

              </article>

            </div>

          </article> 



          <a class="btn-orange next-step" href="resumo-pagamento.php">Continuar</a>   



        </section> 

    </main>
      </div>

   

    <footer class="footer">
      <?php echo $footer;?>  
    </footer>

  
    </section>
  </body>
</html>







