<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="data-certificate">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $loged_in;?>  

      </header>

      <?php echo $search;?>

      <div class="wrapper">
        <main class="main">


        <section class="course-details account-details">


          <article class="course-header">
            
            <div class="course-title">
              <a href="dados-gerais.php">Dados Gerais</a>  
              <a href="emissao-certificado.php" class="active">Dados para Emissão do Certificado</a>
            </div>

          </article>

           <!-- ASIDE -->

          <aside class="data-aside">
            <h1>Rosário Cação</h1>
       
            <img class="profile-img" src="assets/img/user-img/rosario-cacao.png" alt="Rosário Cação">
       
            <a href="#" class="change-prof--img">Alterar Imagem <span class="icon icon-arrow-right"></span></a>

    
  
          </aside>
        

          <article class="data-form">
            
            <div class="data-form--holder">
              <form name="personal-data-post" class="personal-data-form" method="POST" action="#">

                <div class="data-box">
                  <h1>Dados certificado de formação</h1>

                  <div class="input-box">
                    <div class="input-50">
                      <h2>Tipo de documento de identificação</h2>
                      <select name="doc-select" id="doc-select" class="doc-select input-holder">
                        <option value="bi">Bilhete de Identidade</option>
                        <option value="cc">Cartão de Cidadão</option>
                        <option value="passaporte">Passaporte</option>
                      </select>
                    </div>
                    <div class="input-50">
                      <h2>Nº do documento de identificação</h2>
                      <input type="text" name="num-identificacao" placeholder="Nº do documento de identificação" class="input-holder">
                    </div>
                  </div>

                  <div class="input-box">
                    
                    <div class="input-50">
                      <div class="input-33">
                        <h2>Data de Emissão</h2>
                        <input type="text" name="dia" placeholder="Dia" class="input-holder">
                        <input type="text" name="mes" placeholder="Mês" class="input-holder">
                        <input type="text" name="ano" placeholder="Ano" class="input-holder">
                      </div>
                    </div>

                    <div class="input-50">
                      <div class="input-33">
                        <h2>Data de Validade</h2>
                        <input type="text" name="dia" placeholder="Dia" class="input-holder">
                        <input type="text" name="mes" placeholder="Mês" class="input-holder">
                        <input type="text" name="ano" placeholder="Ano" class="input-holder">
                      </div>
                    </div>

                  </div>

                  <div class="input-box">
                    <div class="input-50">
                      <h2>Naturalidade</h2>
                      <input type="text" name="naturalidade-1" placeholder="Naturalidade" class="input-holder">
                    </div>
                    <div class="input-50">
                      <h2>Naturalidade</h2>
                      <input type="text" name="naturalidade-2" placeholder="Naturalidade" class="input-holder">
                    </div>
                  </div>

                  <div class="input-box">
                    <div class="input-50">
                      <h2>Nacionalidade</h2>
                      <input type="text" name="nacionalidade" placeholder="Nacionalidade" class="input-holder">
                    </div>
                    <div class="input-50">
                      <h2>Nº de Contribuinte</h2>
                      <input type="text" name="n-contribuinte" placeholder="Nº de Contribuinte" class="input-holder">
                    </div>
                  </div>

                </div>
 

                <input type="submit" value="Salvar" class="btn-orange btn-save">

              </form>
            </div>

          </article>


          

        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







