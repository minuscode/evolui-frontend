<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="parceiros-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>
      <?php echo $search;?>

      
      <div class="wrapper">
        <div class="banner">
        
      </div>


      <main class="main">


        <section class="course-details">


          <article class="course-header">
            
            <div class="course-title">
              <p>
                O EVOLUI.COM oferece-lhe condições especiais caso seja associado de algum dos nossos parceiros. Para usufruir das condições especiais acordadas, basta utilizar o seu cartão de associado ou fazer prova em como é associado, antes de efectuar o pagamento.
              </p> 
            </div>

          </article>
        

          <ul class="logos-parceiros">
            <li>
              <div class="vertical-align">
                <img src="assets/img/parceiros/cartao-jovem.jpg" alt="Cartão Jovem">
              </div>
              <div class="vertical-align">
                <p>Cartão Jovem</p>
              </div>
            </li>
            <li>
              <div class="vertical-align">
                <img src="assets/img/parceiros/caixa.jpg" alt="Caixa Geral de Depósitos">
              </div>
              <div class="vertical-align">
                <p>Caixa Geral de Depósitos</p>
              </div>
            </li>
            <li>
              <div class="vertical-align">
                <img src="assets/img/parceiros/santander.jpg" alt="Santander Totta">
              </div>
              <div class="vertical-align">
                <p>Santander Totta</p>
              </div>
            </li>
            <li>
              <div class="vertical-align">
                <img src="assets/img/parceiros/apit.jpg" alt="APIT - Associação dos Profissionais da Inspecção Tributária">
              </div>
              <div class="vertical-align">
                <p>APIT - Associação dos Profissionais da Inspecção Tributária</p>
              </div>
            </li>
            <li>
              <div class="vertical-align">
                <img src="assets/img/parceiros/associacao.jpg" alt="APFN - Associação Portuguesa de Famílias Numerosas">
              </div>
              <div class="vertical-align">
                <p>APFN - Associação Portuguesa de Famílias Numerosas</p>
              </div>
            </li>
          </ul>

          <div class="nome-parceiros">
            <h1>E ainda:</h1>
            <ul>
              <li>
                <div>
                  <h1>Empresas de Trabalho Temporário</h1>
                  <p>Nome da Empresa 1</p>
                  <p>Nome da Empresa 2</p>
                  <p>Nome da Empresa 3</p>
                </div>
                <div>
                  <h1>Empresas de Higiene e Segurança no Trabalho</h1>
                  <p>Nome da Empresa 1</p>
                  <p>Nome da Empresa 2</p>
                  <p>Nome da Empresa 3</p>
                  <p>Nome da Empresa 4</p>
                  <p>Nome da Empresa 5</p>
                </div>
              </li>

              <li>
                <div>
                  <h1>Associações de Empresas e de Empresários</h1>
                  <p>Nome da Associação  1</p>
                  <p>Nome da Associação  2</p>
                  <p>Nome da Associação  3</p>
                </div>
                <div>
                  <h1>Associações de Profissionais e Sindicatos</h1>
                  <p>Nome da Associação 1</p>
                </div>
                <div>
                  <h1>Associações de Estudantes</h1>
                  <p>Nome da Associação 1</p>
                  <p>Nome da Associação 2</p>
                </div>
              </li>
            </ul>
          </div>

        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







