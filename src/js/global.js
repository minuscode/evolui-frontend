$(document).ready(function() {


/* fixed menu */

  var $navi = $(".header"), scrollTop = 0;
  $(window).scroll(function(){
    
    
    var y = $(this).scrollTop(), speed = 0.90, pos = y * speed, maxPos = 500;
      if (y > scrollTop) {
        pos = maxPos;
        $navi.addClass("translate");
      } else {
        pos = 0;
        $navi.removeClass("translate");
      }
      scrollTop = y;
    
  });



  /* Dropdrown menu */
 

  var menu = $(".drop-down--menu");
  var btn  = $(".drop-down--btn");


  $('body').on('click', function(a){

    var insideMenu = $(a.target).parents(".drop-down--menu");

    if( insideMenu.length == 0 && !$(a.target).hasClass('drop-down--btn') ){
        menu.removeClass("show-drop-down");
        btn.removeClass("toggled-menu");

    } else {
      a.preventDefault();
      if($(a.target).hasClass('drop-down--btn')){
        if(a.target.tagName != "A") a.preventDefault();
        menu.toggleClass("show-drop-down");
        btn.toggleClass("toggled-menu");
      }
    }
    

  });


/* search box */

  $(".show-search--box").on('click', function(a){

    a.preventDefault();

    if($(".search-engine").hasClass("expand-search")){
      
      $(".search-engine").removeClass("expand-search");
      $(".show-search--box").removeClass("show-search--box--active");
    }


    else{
      $(".search-engine").addClass("expand-search");
      $(".show-search--box").addClass("show-search--box--active");
    }


  });



  /* register */

  $("#register-btn").on('click', function(a){

    a.preventDefault();

    $(".register-holder--overlay").addClass("overlay-in");
    $(".register-form--holder").addClass("form-in");


  });

  $(".close-popup").on('click', function(a){

    a.preventDefault();

    $(".register-holder--overlay").removeClass("overlay-in");
    $(".register-form--holder").removeClass("form-in");

  });


  /* login */

  $("#login-btn").on('click', function(a){

    a.preventDefault();

    $(".login-holder--overlay").addClass("overlay-in");
    $(".login-form--holder").addClass("form-in");


  });

  $(".close-popup").on('click', function(a){

    a.preventDefault();

    $(".login-holder--overlay").removeClass("overlay-in");
    $(".login-form--holder").removeClass("form-in");

  });


  /* ??? */ 

  $(".cancel-course").on('click', function(a){

    a.preventDefault();

    $(".cancel-course--overlay").addClass("overlay-in");
    $(".cancel-box--holder").addClass("form-in");


  });

  $(".cancel").on('click', function(a){

    a.preventDefault();

    $(".cancel-course--overlay").removeClass("overlay-in");
    $(".cancel-box--holder").removeClass("form-in");

  });



/* read more */


var $read_more = $(".read-more");
var $dots = $(".dots");   
  $read_more.click(function(a){

    a.preventDefault();

      var $text = $(".expand-text");
      var $readMore = "Ler mais";
      var $readLess = "Ler menos";
      var $self = $(this);
      var $elem = $(this).parent().find($text);

      if($elem.hasClass("short"))
      {
          $elem.removeClass("short").addClass("full");
          $dots.css("display","none");
          $self.text($readLess);

      }
      else
      {
          $elem.removeClass("full").addClass("short");
          $dots.css("display","inline");
          $self.text($readMore);   
      }       
  });
 


 /* see coments */

var $see_more = $(".see-comments");
  $see_more.click(function(a){

    a.preventDefault();

      var $more_comments = $(".more-comments");
      var $self = $(this);

      if(!$more_comments.hasClass("slideDown"))
      {
          $more_comments.fadeIn(500).addClass("slideDown");
      }
      else
      {
          $more_comments.fadeOut(500).removeClass("slideDown");
      }       
  });


 
    
});


 

























