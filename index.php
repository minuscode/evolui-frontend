<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>
 

  </head>
  <body> 

  <?php include 'include.php';?>

  <section class="home-page">
    
      <?php echo $login;?>
      <?php echo $register;?>
      <?php echo $cancel_course;?>

      <!-- HEADER -->

      <header class="header">
        <?php echo $menu;?>
      </header>

      <div class="wrapper">
        
        <div class="banner">

        <div class="banner-title--holder">
          <hgroup>
            <h1>Bem-vindo ao <span>Evolui.com</span>, o seu site de formação online!</h1>
            <h2>Mais de 200 cursos online certificados e em português.</h2>
          </hgroup>
        </div>

        <div class="search-engine">
          <div class="form-holder">

            <form action="#" method="POST">
              <img class="icon-search1" src="assets/img/icons/search-icon.png" alt="ícon pesquisar">
              <input class="input-style" type="text" name="searchval" placeholder="Qual o curso que procura?">
              <input class="btn-submit" type="submit" value="Pesquisar">

            </form>
            <a href="#">Consulte o nosso catálogo de formação <span class="icon icon-arrow-right"></span></a>
            <a href="#">Como funciona <span class="icon icon-arrow-right"></span></a>

          </div>
        </div>

      </div>

      <section class="slider">
        
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <img src="assets/img/banners/slider-img.jpg" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/img/banners/slider-img.jpg" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/img/banners/slider-img.jpg" alt="">  
                </div>
            </div>
        </div> 
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"><span class="icon icon-arrow-right"></span></div>
        <div class="swiper-button-prev"><span class="icon icon-arrow-left"></span></div>

      </section>

      <main class="main">
        
        
        <h1 class="main-content--title">Cursos</h1>

        <section class="course-holder">

          <article class="course-holder--box">

            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">

            <img src="assets/img/thumbs/qualidade-thumb.jpg" alt="Implementação de Sistemas de Qualidade e Satisfação na Formação">

            <div class="course-title">
              <h1>Implementação de Sistemas de Qualidade e Satisfação na Formação</h1>
            </div>


            <div class="course-price--holder">
              <p class="course-price--discount">€180,00</p>
              <p>€150,00</p>
              <p class="course-date">15 de Janeiro</p>
            </div>

            <div class="course-price">
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Ver detalhes
                </a>
              </div>

            </div>

          </article>

        <!-- 2 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/osteopatia-thumb.jpg" alt="Osteopatia">

          <div class="course-title">
            <h1>Osteopatia</h1>
          </div>

          <div class="course-price--holder">
            <p>€79,72</p>
            <p class="course-date">31 de Janeiro</p>
          </div>

          <div class="course-price">
              
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Ver detalhes
              </a>
            </div>

          </div>

        </article>


        <!-- 3 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/shst-thumb.jpg" alt="Segurança, Higiene e Saúde do Trabalho">

          <div class="course-title">
            <h1>Segurança, Higiene e Saúde do Trabalho</h1>
          </div>

          <div class="course-price--holder">
            <p>€94,90</p>
            <p class="course-date">27 de Janeiro</p>
          </div>

          <div class="course-price">
              
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Ver detalhes
              </a>
            </div>

          </div>

        </article>

        <!-- 4 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/formacao-thumb.jpg" alt="Formação de Formadores em Formato e-Learning">

            <div class="course-title">
              <h1>Formação de Formadores em Formato e-Learning</h1>
            </div>

            <div class="course-price--holder">
              <p>€236,00</p>
              <p class="course-date">26 de Janeiro</p>
            </div>

            <div class="course-price">
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Ver detalhes
                </a>
              </div>

            </div>

          </article>

        <!-- 5 -->

        <article class="course-holder--box">

          <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">

          <img src="assets/img/thumbs/crise-thumb.jpg" alt="Gestão de Crise">

          <div class="course-title">
            <h1>Gestão de Crise</h1>
          </div>

          <div class="course-price--holder">
            <p>€29,90</p>
            <p class="course-date">29 de Janeiro</p>
          </div>

          <div class="course-price">
              
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Ver detalhes
              </a>
            </div>

          </div>

        </article>


        <!-- 6 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/arrendamento-thumb.jpg" alt="Novo Regime do Arrendamento Urbano para a Habitação">

          <div class="course-title">
            <h1>Novo Regime do Arrendamento Urbano para a Habitação</h1>
          </div>

          <div class="course-price--holder">
            <p>€99,90</p>
            <p class="course-date">20 de Fevereiro</p>
          </div>

          <div class="course-price">
              
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Ver detalhes
              </a>
            </div>

          </div>

        </article>

        <a href="#" class="see-all">Ver todos os cursos <span class="icon icon-arrow-right"></span></a>
      </section>



      <!-- ESPECIALIZAÇÕES -->


      <h1 class="main-content--title">Especializações</h1>

        <section class="course-holder specialities">

          <article class="course-holder--box">

            <img src="assets/img/thumbs/processos-thumb.jpg" alt="Curso Integrado de Processos Formativos">

            <div class="course-title">
              <h1>Curso Integrado de Processos Formativos</h1>
            </div>

            <div class="course-price--holder">
              <p>€345,00</p>
            </div>

            <div class="course-price">
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Ver detalhes
                </a>
              </div>

            </div>

          </article>

        <!-- 2 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/geral-thumb.jpg" alt="Curso Geral da Qualidade">

          <div class="course-title">
            <h1>Curso Geral da Qualidade</h1>
          </div>

          <div class="course-price--holder">
            <p>€180,00</p>
          </div>

          <div class="course-price">
              
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Ver detalhes
              </a>
            </div>

          </div>

        </article>


        <!-- 3 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/shst-thumb.jpg" alt="Curso Geral de Qualidade do Ambiente">

          <div class="course-title">
            <h1>Curso Geral de Qualidade do Ambiente</h1>
          </div>

          <div class="course-price--holder">
            <p>€249,90</p>
          </div>

          <div class="course-price">
              
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Ver detalhes
              </a>
            </div>

          </div>

        </article>

        <a href="especializacoes.html" class="see-all">Ver todas as especializações <span class="icon icon-arrow-right"></span></a>
      </section> 

    </main>

    <section class="join-us">

        <h1 class="join-title">Junte-se a nós</h1>

        <ul class="join-us--holder">
          <li>
            <img src="assets/img/icons/icon-variedade.png" alt="Ícon Varidedade">
            <h1>Variedade</h1>
            <p>
              Mais de 200 cursos disponíveis
              das mais diversas áreas de formação
              com materiais de formação atractivos, revistos cientificamente e disponíveis para download
            </p>
          </li>
          <li>
            <img src="assets/img/icons/icon-qualidade.png" alt="Ícon Qualidade">
            <h1>Qualidade comprovada</h1>
            <p>
              Formação certificada pela DGERT e outros organismos e com mais de 65.000
              clientes de cerca de 30 países
            </p>
          </li>
          <li>
            <img src="assets/img/icons/icon-flex.png" alt="Ícon Flexibilidade e Acompanhamento">
            <h1>Flexibilidade e Acompanhamento</h1>
            <p>
              Liberdade de horários, local e ritmo de estudo, tutores permanentemente 
              disponíveis e apoio personalizado
            </p>
          </li>
          <li>
            <img src="assets/img/icons/icon-certificacao.png" alt="Ícon Certificação">
            <h1>Certificação</h1>
            <p>
              Avaliação baseada em testes, trabalhos e colaboração online e certificação das competências adquiridas com registo na caderneta individual de competências
            </p>
          </li>
        </ul>

        <div>
          <a class="join-us--btn" href="#">Registe-se já</a>
          <span>e comece a aprender</span>
        </div>
      </section>

      </div>

      

      <footer class="footer">
        <?php echo $footer;?>
      </footer>

  </section>


  <!-- Swiper JS -->
  <script src="src/js/libs/swiper.jquery.min.js"></script>


  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });
  </script>

     
  </body>
</html>







