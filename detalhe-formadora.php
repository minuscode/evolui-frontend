<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="details-forma-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>

     
  
      <div class="wrapper">
        <main class="main">


        <section class="course-details">

         
          <article class="course-header">
            
            <div class="course-title">
              <img class="photo-forma" src="assets/img/formadores/ana-rita-xavier.png" alt="Ana Rita Xavier">
              <hgroup>
                <h1>Ana Rita Xavier</h1>
                <h2>Formadora</h2>
              </hgroup>
            </div>

          </article>
        

          <article class="course-detail--holder">

            
            <h1>Encara cada dia como um desafio e preserva sempre uma atitude positiva perante a vida.</h1>

            <div class="course-detail--holder_box">
              <h2>Dados Académicos</h2>
              <p>2004 - Curso de Enfermagem na Preparação para o Parto no IFE  Instituto de Formação em Enfermagem, Lda.</p>
              <p>1986 - Especialização em Enfermagem de Saúde Pública da Escola Superior de Enfermagem Maria Fernanda Resende</p>
              <p>1972 - Curso Geral de Enfermagem na Escola Técnica de Enfermeiras do Instituto Português de Oncologia</p>
              <p>1962 - Curso Complementar dos Liceus (alínea F) no Liceu Nacional de Oeiras</p>
            </div>

            <div class="course-detail--holder_box">
              <h2>Dados Profissionais</h2>
              <p>Desde 2009 - Enfermeira responsável pelo projecto "Viver a idade", do ATL da Galiza da Santa Casa da Misericórdia de Cascais</p>
              <p>2007/2009 - Enfermeira responsável pelo projecto Deixai vir na área materno infantil, do ATL da Galiza da Santa Casa da Misericórdia de Cascais</p>
              <p>Desde 2007 - Formadora no EVOLUI.COM</p>
              <p>2002//2011 Formação de estagiárias na área de saúde materno-infantil na Associação Ajuda de Mãe</p>
              <p>2002/2006 - Formadora em cursos de qualificação escolar e profissional em acção educativa, na Associação Ajuda de Mãe</p>
              <p>1976/2006 - Docente de Enfermagem na Escola Superior de Enfermagem de Francisco Gentil</p>
              <p>1974/1976 - Enfermeira no Instituto Português de Oncologia de Lisboa de Francisco Gentil</p>
            </div>


            <div class="course-detail--holder_box">
              <h2>Outros Dados</h2>
              <p>2008 - Formação em criação de recursos didácticos em áudio para e-learning no EVOLUI.COM</p>
              <p>2005 - Formação em criação de conteúdos para e-learning no EVOLUI.COM</p>
              <p>2003 - Formação pedagógica inicial de formadores</p>
              <p>1963 - Inglês Proficiency Certificate no Instituto Britânico em Lisboa</p>
            </div>


          </article>


          <!-- ASIDE -->

          <aside class="course-detail--aside">
            
            <div class="course-detail--aside_box">

              <h2>No Evolui, é formador de:</h2>
              <ul class="clearfix">
                <li>
                  <h3>- A Massagem ao Bebé </h3>
                  <p>(Saúde Infantil) </p>
                  <p>(4 aulas)</p>
                </li>
                <li>
                  <h3>- Saúde Materno Infantil </h3>
                  <p>(6 aulas)</p>
                </li>
              </ul>
            </div>

  
          </aside>

        </section> 

    </main>

      </div>
   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







