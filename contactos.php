<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="contactos-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>
       <?php echo $search;?>

      <div class="wrapper">
        <div class="banner">
       
      </div>


      <main class="main">


        <section class="course-details">


          <article class="course-header">
            
            <div class="course-title">
              <p>
                Para obter mais informações sobre os cursos do EVOLUI.COM, entre em contacto connosco pelos seguintes meios:
              </p> 
            </div>

          </article>
        

          <ul class="contacts-list">
            <li>
              <div class="contacts-list--holder">
                <span class="icon icon-phone"></span>
                <hgroup>
                  <h1>Telefone</h1>
                  <h2>+351 233 412 315</h2>
                  <p>(dias úteis das 9h às 18h)</p>
                </hgroup>
              </div>

              <div class="contacts-list--holder">
                <span class="icon icon-printer"></span>
                <hgroup>
                  <h1>Fax</h1>
                  <h2>+351 233 412 317</h2>
                </hgroup>
              </div>
            </li>

            <li>
              <div class="contacts-list--holder">
                <span class="icon icon-mailer"></span>
                <hgroup>
                  <h1>E-mail</h1>
                  <h2>info@evolui.com</h2>
                </hgroup>
              </div>
              
            </li>

            <li>
              <div class="contacts-list--holder">
                <span class="icon icon-marker"></span>
                <hgroup>
                  <h1>Morada</h1>
                  <h2>Ilha da Morraceira</h2>
                  <p>
                    À antiga seca do bacalhau<br>
                    3090-707 Figueira da Foz<br>
                    Portugal
                  </p>
                </hgroup>
              </div>
              
            </li>
            
          </ul>

           

        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







