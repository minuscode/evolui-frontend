<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="data-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $loged_in;?>  

      </header>

      <?php echo $search;?>

      

      <div class="wrapper">
        <main class="main">


        <section class="course-details account-details">


          <article class="course-header">
            
            <div class="course-title">
              <a href="dados-gerais.php" class="active">Dados Gerais</a>  
              <a href="emissao-certificado.php">Dados para Emissão do Certificado</a>
            </div>

          </article>

           <!-- ASIDE -->

          <aside class="data-aside">
            <h1>Rosário Cação</h1>
       
            <img class="profile-img" src="assets/img/user-img/rosario-cacao.png" alt="Rosário Cação">
       
            <a href="#" class="change-prof--img">Alterar Imagem <span class="icon icon-arrow-right"></span></a>

    
  
          </aside>
        

          <article class="data-form">
            
            <div class="data-form--holder">
              <form name="personal-data-post" class="personal-data-form" method="POST" action="#">

                <div class="data-box">
                  <h1>Dados Pessoais</h1>

                  <div class="input-box">
                    <div class="input-50">
                      <input type="text" name="nome" placeholder="Nome" class="input-holder">
                    </div>
                    
                    <div class="input-50">
                      <input type="text" name="sobrenome" placeholder="Sobrenome" class="input-holder">
                    </div>
                  </div>

                  <div class="input-box">
                    <div class="input-100">
                      <input type="text" name="morada" placeholder="Morada" class="input-holder">
                    </div>
                  </div>

                  <div class="input-box">
                    <div class="input-50">
                      <input type="text" name="cod-postal" placeholder="Código Postal" class="input-holder">
                    </div>
                    <div class="input-50">
                      <input type="text" name="localidade" placeholder="Localidade" class="input-holder"> 
                    </div>
                  </div>

                  <div class="input-box">
                    <div class="input-50">
                      <input type="text" name="pais" placeholder="País" class="input-holder">
                    </div>
                    <div class="input-50">
                      <input type="text" name="tel" placeholder="Telefone / Telemóvel" class="input-holder">
                    </div>                    
                  </div>

                  <div class="input-box">
                    <div class="input-50">
                      <input type="text" name="email" placeholder="E-mail" class="input-holder">
                    </div>
                  </div>
                </div>

                <div class="data-box">
                  <h1>Dados de acesso</h1>

                  <div class="input-box">
                    <div class="input-50">
                      <input type="text" name="nome-membro" placeholder="Nome de membro" class="input-holder">
                    </div>
                  </div>

                  <div class="input-box">
                    <div class="input-50">
                      <input type="password" name="password" placeholder="Password" class="input-holder">
                    </div>
                    <div class="input-50">
                      <input type="password" name="password" placeholder="Confirme a sua password" class="input-holder">
                    </div>
                  </div>

                </div>

                <div class="checkbox-holder">
                  <input class="check-box" type="checkbox" id="terms" name="checkbox-terms" value="none">
                  <label for="terms" class="checkbox-terms--text">Desejo receber as novidades e promoções do EVOLUI.COM</label>
                </div>

                <input type="submit" value="Salvar" class="btn-orange btn-save">

              </form>
            </div>

          </article>


          

        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







