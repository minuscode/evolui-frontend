<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="qualidade-ambiente-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>
      <?php echo $search;?>

      

      <div class="wrapper">
        <div class="banner">
        
      </div>


      <main class="main">


        <section class="course-details">


          <article class="course-header">
            
            <div class="course-title">
              <h1>A Actividade de Transporte e a Logística</h1>  
            </div>

            <div class="course-favorite">
              <a href="#" class="btn-orange">Inscreva-se neste curso</a>
              <a href="#" class="add-favorite"><span class="icon icon-star"></span>Marcar como favorito</a>
            </div>

          </article>
        

          <article class="course-detail--holder">

          
            <div class="course-detail--holder_box">
              <h2>Resumo</h2>
              <p>
                Este curso integra módulos relacionados com as questões ambientes e inclui desde as metodologias para realizar diagnósticos ambientais até à implementação de sistemas de gestão ambiental e sistemas integrados de gestão da qualidade, ambiente e segurança. 
              </p>
            </div>

            <div class="course-detail--holder_box">
              <h2>Funcionamento</h2>
              <p>Este curso é composto por vários módulos podendo iniciar já todos os módulos ou desfasando alguns no tempo. Cada módulo tem condições de funcionamento e requisitos específicos que deve verificar antes de se inscrever. Durante a duração de cada módulo, tem acesso a todas as aulas do módulo e ao fórum de discussão onde irá trabalhar as tarefas atribuídas pelo formador desse módulo e onde poderá esclarecer as suas dúvidas. Os módulos do curso são autónomos pelo que no final do curso receberá um certificado de formação profissional por cada módulo em que tiver aproveitamento.</p>
            </div>


            <div class="table-holder">
              <table>
                <tr>
                  <th><span class="icon icon-mod"></span><p>Módulos do curso</p></th>
                  <th><span class="icon icon-lessons"></span><p>Nrº de Aulas</p></th>
                  <th><span class="icon icon-date"></span><p>Próxima Edição</p></th>
                  <th><span class="icon icon-type"></span><p>Tipo</p></th>
                  <th><span class="icon icon-price"></span><p>Preço normal</p></th>
                </tr>

                <tr>
                  <td>Diagnósticos Ambientais</td>
                  <td>9</td>
                  <td>28 de Janeiro</td>
                  <td>Curso com formador</td>
                  <td>€99,90</td>
                </tr>

                <tr>
                  <td>Sistemas Integrados de Gestão de Qualidade, Ambiente e Segurança</td>
                  <td>11</td>
                  <td>15 de Janeiro</td>
                  <td>Curso com formador</td>
                  <td>€150,00</td>
                </tr>

                <tr>
                  <td>Implementação de Sistemas de Gestão Ambiental</td>
                  <td>8</td>
                  <td>15 de Janeiro</td>
                  <td>Curso com formador</td>
                  <td>€120,00</td>
                </tr>

                <tr>
                  <td class="hidden"></td>
                  <td class="hidden"></td>
                  <td class="hidden"></td>
                  <td>Total</td>
                  <td>€369,90</td>
                </tr>
  
                <tr>
                  <td class="hidden"></td>
                  <td class="hidden"></td>
                  <td class="hidden"></td>
                  <td>Preço Promocional</td>
                  <td>€249,90</td>
                </tr>

              </table>
            </div>


            <a href="#" class="btn-orange">Inscreva-se neste curso</a>

          </article>



        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







