
<?php


$menu='

	<div class="main">
	        
	  <a class="logo" href="index.php"><img src="assets/img/icons/logo.png" alt="Logo Evolui"></a>

	  <nav class="log-nav">
	    <ul>
	      <li><a href="#" id="login-btn">Iniciar Sessão</a></li>
	      <li><a href="#" id="register-btn">Registe-se</a></li>
	    </ul>
	  </nav>

	  <a class="show-search--box" href="#"> <img class="icon-search1" src="assets/img/icons/search-icon.png" alt="ícon pesquisar"> </a>

	  <nav class="main-nav">
	    <ul>
	      <li class="drop-down"><a class="drop-down--btn" href="#">Cursos</a></li>
	      <li><a class="link-especializacoes" href="especializacoes.php">Especializações</a></li>
	    </ul>
	  </nav>
	  
	  <div class="drop-down--menu">
      <div class="menu-wrap clearfix">
        <h1>Áreas de Formação</h1>

          <ul>
            <li><a href="#">Comunicação e Línguas</a></li>
            <li><a href="#">Desenvolvimento Pessoal</a></li>
            <li><a href="#">Design e Imagem</a></li>
            <li><a href="regulamento.php">Direito e Regulamentação Familia</a></li>
            <li><a href="#">Ferramentas Informáticas</a></li>
            <li><a href="#">Finanças e Mercados</a></li>
            <li><a href="gestao-comercial.php">Gestão Comercial e de Operações</a></li>
          </ul>
          <ul>
            <li><a href="#">Gestão de Recursos Humanos</a></li>
            <li><a href="#">Gestão e Negócios</a></li>
            <li><a href="#">Marketing</a></li>
            <li><a href="#">Novas Tecnologias na Educação</a></li>
            <li><a href="#">Pedagogia</a></li>
            <li><a href="#">Pedagogia da Infância e da Adolescência</a></li>
          </ul>
          <ul>
            <li><a href="#">Psicologia</a></li>
            <li><a href="#">Qualidade, Ambiente e Segurança</a></li>
            <li><a href="#">Saúde e Bem-Estar</a></li>
            <li><a href="#">Sistemas de Informação e Programação</a></li>
            <li><a href="#">Turismo e Lazer</a></li>
            <li><a href="#">Todos os cursos</a></li>
          </ul>
      </div>
	    
	  </div>
	  

	</div>

';



$loged_in='

  <div class="main">
          
    <a href="index.php"><img class="logo" src="assets/img/icons/logo.png" alt="Logo Evolui"></a>

    <nav class="loged-nav">
      <ul>
        <li><img src="assets/img/acc-img/rosario-cacao.png" alt=""></li>
        <li><a href="#">Rosário Cação <span class="icon icon-arrow-down"></span></a></li>
      </ul>
    </nav>

    <a class="show-search--box" href="#"> <img class="icon-search1" src="assets/img/icons/search-icon.png" alt="ícon pesquisar"> </a>

    <nav class="main-nav">
      <ul>
        <li class="drop-down"><a class="drop-down--btn" href="#">Cursos</a></li>
        <li><a class="link-especializacoes" href="especializacoes.php">Especializações</a></li>
      </ul>
    </nav>
    
    <div class="drop-down--menu">
      <div class="menu-wrap clearfix">
        <h1>Áreas de Formação</h1>

          <ul>
            <li><a href="#">Comunicação e Línguas</a></li>
            <li><a href="#">Desenvolvimento Pessoal</a></li>
            <li><a href="#">Design e Imagem</a></li>
            <li><a href="regulamento.php">Direito e Regulamentação Familia</a></li>
            <li><a href="#">Ferramentas Informáticas</a></li>
            <li><a href="#">Finanças e Mercados</a></li>
            <li><a href="gestao-comercial.php">Gestão Comercial e de Operações</a></li>
          </ul>
          <ul>
            <li><a href="#">Gestão de Recursos Humanos</a></li>
            <li><a href="#">Gestão e Negócios</a></li>
            <li><a href="#">Marketing</a></li>
            <li><a href="#">Novas Tecnologias na Educação</a></li>
            <li><a href="#">Pedagogia</a></li>
            <li><a href="#">Pedagogia da Infância e da Adolescência</a></li>
          </ul>
          <ul>
            <li><a href="#">Psicologia</a></li>
            <li><a href="#">Qualidade, Ambiente e Segurança</a></li>
            <li><a href="#">Saúde e Bem-Estar</a></li>
            <li><a href="#">Sistemas de Informação e Programação</a></li>
            <li><a href="#">Turismo e Lazer</a></li>
            <li><a href="#">Todos os cursos</a></li>
          </ul>
      </div>
      
    </div>
    

  </div>

';


$login='
  
  <section class="login-holder--overlay">

    <article class="login-form--holder">

      <a href="#" class="icon icon-close-2 close-popup"></a>

      <form id="register-form" action="#">  

        <h1>Iniciar sessão</h1>

        <div class="input-holder">
          <span class="icon icon-mail-1"></span>
          <input class="input-style" type="email" name="email" placeholder="Indique o seu e-mail">
        </div>

        <div class="input-holder">
          <span class="icon icon-password"></span>
          <input class="input-style" type="password" name="password" placeholder="Indique a sua password">
        </div>

        <a href="#" class="reset-password">Esqueci-me da password.</a>
        

        <input class="btn-orange" type="submit" value="Submeter">


        <div class="fb-login">
          
          <span class="other">ou</span>


          <a href="#" class="fb-login--link">
            <span class="icon icon-facebook"></span>
            <p>Iniciar sessão com o Facebook</p>
          </a>


        </div>

      </form>

      <div class="have-acc--box">
        <p>Já tem uma conta? <a href="#">Iniciar sessão</a></p>
      </div>


    </article>

  </section>

';



$register='
  
  <section class="register-holder--overlay">

    <article class="register-form--holder">

      <a href="#" class="icon icon-close-2 close-popup"></a>

      <form id="register-form" action="#">  

        <h1>Torne-se membro</h1>

        <div class="input-holder">
          <span class="icon icon-username"></span>
          <input class="input-style" type="text" name="nome" placeholder="Indique o seu nome">
        </div>

        <div class="input-holder">
          <span class="icon icon-mail-1"></span>
          <input class="input-style" type="email" name="email" placeholder="Indique o seu e-mail">
        </div>

        <div class="input-holder">
          <span class="icon icon-password"></span>
          <input class="input-style" type="password" name="password" placeholder="Indique a sua password">
        </div>

        <div class="checkbox-holder">
          <input class="check-box" type="checkbox" id="terms" name="checkbox-terms" value="none">
          <label for="terms" class="checkbox-terms--text">Li e concordo com os <a href="#" class="terms">termos e condições</a> do site.</label>
        </div>
        

        <input class="btn-orange" type="submit" value="Submeter">


        <div class="fb-login">
          
          <span class="other">ou</span>


          <a href="#" class="fb-login--link">
            <span class="icon icon-facebook"></span>
            <p>Iniciar sessão com o Facebook</p>
          </a>


        </div>

      </form>

      <div class="have-acc--box">
        <p>Já tem uma conta? <a href="#">Iniciar sessão</a></p>
      </div>


    </article>

  </section>

';

$search='

	<div class="search-engine">
    <div class="form-holder">

      <form action="#" method="POST">
        <img class="icon-search1" src="assets/img/icons/search-icon.png" alt="ícon pesquisar">
        <input class="input-style" type="text" name="searchval" placeholder="Qual o curso que procura?">
        <input class="btn-submit" type="submit" value="Pesquisar">

      </form>
      <a href="#">Consulte o nosso catálogo de formação <span class="icon icon-arrow-right"></span></a>

    </div>
  </div>

';

/*

$order_catalogue='

	<div class="order-catalogue--holder">
	  <h1>Faça a sua pesquisa de acordo com os parâmetros abaixo</h1>
	  <ul>
	    <li><a class="active" href="#">Ordem alfabética</a></li>
	    <li><a href="#">Data de início</a></li>
	    <li><a href="#">Novos</a></li>
	    <li><a href="#">Promoções</a></li>
	    <li><a href="#">Especializações</a></li>
	    <li><a class="link-catalogo" href="catalogo.php"><span class="icon icon-grid-1"></span></a></li>
	    <li><a class="link-cat-listagem" href="catalogo-listagem.php"><span class="icon icon-list-1"></span></a></li>
	  </ul>
	</div>

';

*/



$footer='

	<main class="main">
    
      <ul>
        <li>
          <h1>Áreas de Formação</h1>
            <ul>
              <li><a href="#">- Comunicação e Línguas</a></li>
              <li><a href="#">- Desenvolvimento Pessoal</a></li>
              <li><a href="#">- Design e Imagem</a></li>
              <li><a href="regulamento.php">- Direito e Regulamentação Familia</a></li>
              <li><a href="#">- Ferramentas Informáticas</a></li>
              <li><a href="#">- Finanças e Mercados</a></li>
              <li><a href="gestao-comercial.php">- Gestão Comercial e de Operações</a></li>
            </ul>
            <ul>
              <li><a href="#">- Gestão de Recursos Humanos</a></li>
              <li><a href="#">- Gestão e Negócios</a></li>
              <li><a href="#">- Marketing</a></li>
              <li><a href="#">- Novas Tecnologias na Educação</a></li>
              <li><a href="#">- Pedagogia</a></li>
              <li><a href="#">- Pedagogia da Infância e da Adolescência</a></li>
            </ul>
            <ul>
              <li><a href="#">- Psicologia</a></li>
              <li><a href="#">- Qualidade, Ambiente e Segurança</a></li>
              <li><a href="#">- Saúde e Bem-Estar</a></li>
              <li><a href="#">- Sistemas de Informação e Programação</a></li>
              <li><a href="#">- Turismo e Lazer</a></li>
            </ul>
        </li>
        <li class="border">
          <h1>Informações Gerais</h1>
            <ul>
              <li><a href="catalogo.php">Catálogo de Cursos</a></li>
              <li><a href="#">Perguntas Frequentes</a></li>
              <li><a href="#">Condições de Serviço</a></li>
              <li><a href="#">Contactos</a></li>
              <li><a href="#">Parcerias</a></li>
            </ul>
        </li>
        <li>
          <h1>Siga-nos</h1>
          <ul>
            <li><a href="#"><span class="icon icon-facebook"></span></a></li>
            <li><a href="#"><span class="icon icon-instagram"></span></a></li>
            <li><a href="#"><span class="icon icon-pinterest"></span></a></li>
            <li><a href="#"><span class="icon icon-linkedin"></span></a></li>
          </ul>
          <h1>Subscreva a nossa newsletter</h1>

            <form class="newsletter-form" action="#" method="POST">
              <span class="icon icon-mail-2"></span>
              <input class="input-style" type="email" name="email" placeholder="Indique o seu email">
               <button type="submit">
                  <span class="icon icon-arrow-right"></span>
              </button>
            </form>
        </li>
      </ul>
    

    </main>

';



$cancel_course='
  
  <section class="cancel-course--overlay">

    <article class="cancel-box--holder">

      <h1>
        Tem a certeza que deseja cancelar a inscrição no curso “A Actividade de Transporte e a Logística”?
      </h1>

      <ul>
        <li><a class="cancel" href="#">Cancelar</a></li>
        <li><a class="confirm" href="#">Confirmar</a></li>
      </ul>


    </article>

  </section>

';



