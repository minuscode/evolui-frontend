<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

  <?php include 'include.php';?> 



  <section class="gestao-comercial-page">
    

      <!-- HEADER -->

      <header class="header">
        <?php echo $menu;?>
      </header>
      <?php echo $search;?>

      
      <div class="wrapper">
        <div class="banner">
        

        <div class="banner-title--holder">
          <hgroup>
            <h1>Gestão Comercial e de Operações</h1>
            <h2>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam
eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
            </h2>
          </hgroup>
        </div>
      </div>

      

      <main class="main">
        <section class="course-holder">


          <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/transportes-thumb.jpg" alt="A Actividade de Transporte e a Logística">

            <div class="course-title">
              <h1>A Actividade de Transporte e a Logística</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€74,90</p>
                <p class="course-date">18 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  A Actividade de Transporte e a Logística
                </a>
              </div>

            </div>

          </article>

        <!-- 2 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/administracao-thumb.jpg" alt="Administração de Imóveis">

            <div class="course-title">
              <h1>Administração de Imóveis</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€69,90</p>
                <p class="course-date">31 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Administração de Imóveis
                </a>
              </div>

            </div>

          </article>


        <!-- 3 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/angariar-thumb.jpg" alt="Angariação Imobiliária">

            <div class="course-title">
              <h1>Angariação Imobiliária</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€39,90</p>
                <p class="course-date">22 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Angariação Imobiliária
                </a>
              </div>

            </div>

          </article>

        <!-- 4 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/bemvindo-thumb.jpg" alt="Bem-vindo ao EVOLUI.COM">

            <div class="course-title">
              <h1>Bem-vindo ao EVOLUI.COM</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€00,00</p>
                <p class="course-date">17 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Bem-vindo ao EVOLUI.COM
                </a>
              </div>

            </div>

          </article>

        <!-- 5 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/cathering-thumb.jpg" alt="Catering e Protocolo">

            <div class="course-title">
              <h1>Catering e Protocolo</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€39,90</p>
                <p class="course-date">1 de Fevereiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Catering e Protocolo
                </a>
              </div>

            </div>

          </article>

        <!-- 6 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/franchising-thumb.jpg" alt="Franchising">

            <div class="course-title">
              <h1>Franchising</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€84,90</p>
                <p class="course-date">23 de Fevereiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Franchising
                </a>
              </div>

            </div>

          </article>

        <!-- 7 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/residuos-thumb.jpg" alt="Gestão de Resíduos">

            <div class="course-title">
              <h1>Gestão de Resíduos</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€89,90</p>
                <p class="course-date">25 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Gestão de Resíduos
                </a>
              </div>

            </div>

          </article>

        <!-- 8 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/vendas-thumb.jpg" alt="Gestão de Vendas">

            <div class="course-title">
              <h1>Gestão de Vendas</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€94,90</p>
                <p class="course-date">25 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Gestão de Vendas
                </a>
              </div>

            </div>

          </article>

        <!-- 9 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/gestao-comercial/aprovisionamento-thumb.jpg" alt="Gestão de Aprovisionamento">

          <div class="course-title">
            <h1>Gestão de Aprovisionamento</h1>
          </div>


          <div class="course-price">
            <div class="course-price--holder">
              <p>€99,90</p>
              <p class="course-date">29 de Janeiro</p>
            </div>
            
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Gestão de Aprovisionamento
              </a>
            </div>

          </div>

        </article>


      </section> 
    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>
      </footer>


     </section>
  </body>
</html>







