<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="my-acc-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $loged_in;?>  

      </header>

      <?php echo $search;?>

      

      <div class="wrapper">
        <main class="main">


        <section class="course-details account-details">


          <article class="course-header">
            
            <div class="course-title">
              <h1>Minha conta</h1>  
            </div>

          </article>

          <div class="notice-box">
            <span class="icon icon-info"></span>
            <p>Para efeitos de emissão dos certificados de formação profissional, actualize os seus dados pessoais.</p>
          </div>
        

          <article class="course-detail--holder">

            

            <div class="course-detail--holder_box">

              <table class="acc-table">
                <tr>
                  <th colspan="5">Pagamentos por efectuar</th>
                </tr>
                <tr>
                  <td>Inicio</td>
                  <td>Fim</td>
                  <td>Curso</td>
                  <td>Preço</td>
                  <td></td>
                </tr>

                <tr>
                  <td>18-Abr</td>
                  <td>2-Mai</td>
                  <td>Gestão de Formação</td>
                  <td>€99,90</td>
                  <td rowspan="3"><a href="#" class="aleo-reg-16">Dados de Pagamento <span class="icon icon-arrow-right"></span></a></td>
                </tr>

                <tr>
                  <td>20-Abr</td>
                  <td>30-Abr</td>
                  <td>Introdução ao Linux</td>
                  <td>€79,99</td>
                </tr>

                <tr>
                  <td>30-Abr</td>
                  <td>10-Mai</td>
                  <td>Gestão de Crises</td>
                  <td>€40</td>
                </tr>

                <tr>
                  <td>18-Abr</td>
                  <td>2-Mai</td>
                  <td>Gestão de Formação</td>
                  <td>€99,90</td>
                  <td><a href="#" class="aleo-reg-16">Dados de pagamento <span class="icon icon-arrow-right"></span></a></td>
                </tr>
                
              </table>
              
            </div> 


            <div class="course-detail--holder_box">

              <table class="acc-table">
                <tr>
                  <th colspan="5">Pedidos de inscrição</th>
                </tr>
                <tr>
                  <td>Inicio</td>
                  <td>Fim</td>
                  <td>Curso</td>
                  <td>Preço</td>
                  <td></td>
                </tr>

                <tr>
                  <td>15-Mar</td>
                  <td>30-Mar</td>
                  <td>Psicoterapia</td>
                  <td>€79,90</td>
                  <td rowspan="4"><a href="#" class="aleo-reg-16">Completar<span class="icon icon-arrow-right"></span></a></td> 

                </tr>

                <tr class="edit-table--links">
                  <td colspan="4"><a href="#">Alterar Data</a> | <a href="#">Guardar para mais tarde</a></td>
                </tr>

                <tr>
                  <td>10-Mar</td>
                  <td>30-Mar</td>
                  <td>Socorrismo</td>
                  <td>€49,99</td>
                </tr>

                <tr class="edit-table--links">
                  <td colspan="4"><a href="#">Alterar Data</a> | <a href="#">Guardar para mais tarde</a></td>
                </tr>

                <tr class="saved-course">
                  <td colspan="5">Cursos guardados para mais tarde</td>
                </tr>

                <tr>
                  <td>15-Mar</td>
                  <td>30-Mar</td>
                  <td>Psicoterapia</td>
                  <td class="remove-border--right">€79,90</td> 

                </tr>

                <tr class="edit-table--links">
                  <td colspan="4" class="rem-after"><a href="#">Mover para os favoritos</a> | <a href="#">Mover para os pedidos de inscrição</a></td>
                </tr>

                <tr>
                  <td>10-Mar</td>
                  <td>30-Mar</td>
                  <td>Socorrismo</td>
                  <td class="remove-border--right">€49,99</td>
                </tr>

                <tr class="edit-table--links">
                  <td colspan="4" class="remove-border--top rem-after"><a href="#">Mover para os favoritos</a> | <a href="#">Mover para os pedidos de inscrição</a></td>
                </tr>
 
                
              </table>
              
            </div> 




            <div class="course-detail--holder_box">

              <table class="course-table acc-table remove-table--border">
                <tr>
                  <th colspan="5">Cursos a decorrer</th>
                </tr>
                <tr>
                  <td>Inicio</td>
                  <td>Fim</td>
                  <td>Curso</td>
                  <td>Preço</td>
                  <td>Actividade</td>
                </tr>

                <tr>
                  <td>1-Mar</td>
                  <td>30-Mar</td>
                  <td class="underline">Gestão da Formação</td>
                  <td>€99,90</td>
                  <td><a href="#" class="in-sms">x mensagens novas</a></td>
                </tr>

                <tr>
                  <td>28-Fev</td>
                  <td>30-Mar</td>
                  <td class="underline">Introdução ao Linux</td>
                  <td>€79,90</td>
                  <td><a href="#" class="in-sms">x mensagens novas</a></td>
                </tr>

                <tr>
                  <td>10-Mai</td>
                  <td>5-Jun</td>
                  <td class="underline">Gestão de Crises</td>
                  <td>€40</td>
                  <td>Ainda não começou</td>
                </tr>
                
                <tfoot>
                  <tr>
                    <td colspan="5">
                      <a href="#" class="aleo-reg-16">Ver mais</a>
                    </td>
                  </tr>
                </tfoot>

              </table>
              
            </div> 



            <div class="course-detail--holder_box">

              <table class="course-table acc-table remove-table--border">
                <tr>
                  <th colspan="5">Cursos a terminados</th>
                </tr>
                <tr>
                  <td>Inicio</td>
                  <td>Fim</td>
                  <td>Curso</td>
                  <td>Classificação</td>
                  <td class="center-text">Certificado</td>
                </tr>

                <tr>
                  <td>1-Mar</td>
                  <td>30-Mar</td>
                  <td class="underline">Fotografia Digital</td>
                  <td class="in-sms">Muito Bom</td>
                  <td class="center-text"><a href="#" class="in-sms"><span class="icon icon-download"></span></a></td>
                </tr>

                <tr>
                  <td>28-Fev</td>
                  <td>20-Mar</td>
                  <td class="underline">Introdução ao Linux</td>
                  <td class="in-sms">Suficiente</td>
                  <td class="center-text"><a href="#" class="in-sms"><span class="icon icon-download"></span></a></td>
                </tr>
                
                <tfoot>
                  <tr>
                    <td colspan="5">
                      <a href="#" class="aleo-reg-16">Ver mais</a>
                    </td>
                  </tr>
                </tfoot>

              </table>
              
            </div> 

         

          </article>


          <!-- ASIDE -->

          <aside class="course-detail--aside">

            <div class="course-detail--aside_box course-sugestion--holder">

              <h2>Próximas Datas</h2>
              <ul class="clearfix">
                <li>
                  <img src="assets/img/formadores/course-sugestion.jpg" alt="">
                  <img src="assets/img/formadores/course-sugestion-2.jpg" alt="">
                </li>
              </ul>
            </div>

            <div class="course-detail--aside_box course-sugestion--holder">

              <h2>Favoritos</h2>
              <ul class="clearfix">
                <li>
                  <a href="#" class="manage-favorite">Gerir Favoritos</a>
                  <img src="assets/img/formadores/course-sugestion.jpg" alt="">
                  <h2>Osteopatia</h2>
                  <div><span>€79,90</span> <span>31 de Janeiro</span></div>
                  <div>

                    <a href="#" class="aleo-reg-16 sign-up">Inscreva-se<span class="icon icon-arrow-right"></span></a>
                  </div>
                </li>
              </ul>
            </div>

  
          </aside>

        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







