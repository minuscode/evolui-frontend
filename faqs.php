<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>


      <script>
          $(document).ready(function() {     
                  
                var 
                  $accordion = $(".accordion"),
                  $allPanels = $('> dd', $accordion), 
                  $toggle_plus = $(".icon-arrow-right", $accordion),
                  $dt_holder = $(".dt-holder", $accordion);

                $allPanels.hide();

                $('> dt > a', $accordion).click(function() {
                    var
                      $self = $(this), $target = $self.parent().next(), $holder = $self.parent(),
                      $self_toggle_plus = $(".icon-arrow-right", $holder);

                    $dt_holder.removeClass("activeDD");
                    $toggle_plus.removeClass("flipAfter");

                    if($accordion.find(".active").length > 0){
                      $allPanels.slideUp();
                    }

                    if($target.hasClass('active')){
                      $allPanels.removeClass('active');
                      $allPanels.slideUp();
                    }
                    else{
                      $holder.addClass('activeDD');
                      $allPanels.removeClass('active');
                      $target.slideDown(function(){
                          //$('html,body').animate({scrollTop: $(this).offset().top -100},'slow');
                      }).addClass('active');
                      $self_toggle_plus.addClass('flipAfter');
                    }


                  return false;
                });

            });
        </script>
 

  </head>
  <body> 

  <?php include 'include.php';?>

    <section class="faqs-page">
      
    
      <!-- HEADER -->

      <header class="header">
        <?php echo $menu;?>
      </header>

      

      <div class="wrapper">
        <main class="main">
  

        <section class="course-holder">

          <div class="course-title">
            <h1>Perguntas Frequentes</h1>
          </div>
          
          <article class="faqs-holder">
            <h1 class="faqs-title">
              1. O Evolui
            </h1>
            
            <ul class="faqs-holder--list">
              <li>
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>O que é o EVOLUI.COM?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Que vantagens oferece EVOLUI.COM? </h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Que tipo de cursos existem no EVOLUI.COM? </h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1> Como posso ser formador? </h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>
  
              </li>


              <!-- 2 -->


              <li>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso contactar o EVOLUI.COM? </h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl> 
              
                 
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Os cursos do EVOLUI.COM estão certificados? </h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl> 

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1> Existe um regulamento de formação? </h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>


              </li>
              
                
            </ul>
          </article>

          <!-- MEMBROS -->

          <article class="faqs-holder">
            <h1 class="faqs-title">
              2. Membros
            </h1>
            
            <ul class="faqs-holder--list">


              <li class="half-50">
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso tornar-me Membro?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Que vantagens tenho em me tornar Membro?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso alterar as informações do meu registo?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>
              </li>

            </ul>
          </article>


          <!-- INSCRIÇÕES -->

          <article class="faqs-holder">
            <h1 class="faqs-title">
              3. Inscrições
            </h1>
            
            <ul class="faqs-holder--list">


              <li>
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso inscrever-me num curso ou num tutorial?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso completar a minha inscrição?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso alterar a data de início?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>
              
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Posso cancelar a minha inscrição?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Posso-me inscrever depois do curso começar?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

              </li>

              <!-- 2 -->
    
              <li>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Vivo fora de Portugal. Posso frequentar um curso?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>É preciso ter um computador especial para frequentar os cursos?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Existe um número mínimo de participantes?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>
                
              </li>  

            </ul>
          </article>

          <!-- PAGAMENTOS -->

          <article class="faqs-holder">
            <h1 class="faqs-title">
              4. Pagamentos
            </h1>
            
            <ul class="faqs-holder--list">


              <li>
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso pagar?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Quando é que tenho de pagar a minha inscrição?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso usufruir de um vale de desconto que recebi?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>
              
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso usufruir do desconto de Cartão Jovem?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Estou fora de Portugal. Como posso pagar?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

              </li>

              <!-- 2 -->
    
              <li>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Quanto tenho de pagar?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Posso desistir ou cancelar a minha inscrição?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso usufruir do desconto de Associado de um parceiro?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>
                
              </li>  

            </ul>
          </article>

          <!-- FREQUENTAR UM CURSO -->

          <article class="faqs-holder">
            <h1 class="faqs-title">
              5. Frequentar um curso
            </h1>
            
            <ul class="faqs-holder--list">


              <li>
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Posso receber um Certificado de Formação Profissional?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como é que frequento um curso?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como é feita a avaliação?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>
              
                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Como posso guardar os materiais da formação?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

              </li>

              <!-- 2 -->
    
              <li>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>O que posso fazer se não terminar o curso dentro do prazo previsto?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Posso solicitar uma segunda via do certificado?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>

                <dl class="accordion">
                  <dt class="dt-holder">
                    <a href="#">
                      <span class="icon icon-arrow-right"></span>
                      <h1>Qual é a escala em que vou ser avaliado?</h1>
                    </a>
                  </dt>
                  <dd>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non excepturi, neque consequuntur magni distinctio ut earum veniam molestiae quam, ratione quibusdam voluptatem. Cumque ducimus dicta fuga atque odit alias eligendi!</p>
                  </dd>
                </dl>
                
              </li>  

            </ul>
          </article>
          
         

        </section> 


      </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>
      </footer>


    </section>
  </body>
</html>







