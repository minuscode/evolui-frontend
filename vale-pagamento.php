<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="pay-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>
      <?php echo $search;?>


      <div class="wrapper">
        <main class="main">


        <section class="pay-details--holder">


          <article class="pay-progress">
            
            <ul class="pay-progress--holder">
              <li>
                <div class="circle">
                  <span class="icon icon-check"></span>  
                </div>
                <p class="desc">Detalhes de Compra</p>
              </li>
              <li>
                <div class="circle">
                  <span class="icon icon-check"></span>  
                </div>
                <p class="desc">Confirmação</p>
              </li>
              <li class="active">
                <div class="circle">
                  <span>3</span>  
                </div>
                <p class="desc">Resumo</p>
              </li>
            </ul>

          </article>
        
 
          <article class="pay-holder">

            <div class="pay-holder--box">
              <table class="pay-table">
                <thead>
                  <tr>
                    <th>
                      <div>
                        <h2>Descrição</h2>
                      </div>
                    </th>
                    <th>
                      <div>
                        <h2>Preço</h2>
                      </div>
                    </th>
                  </tr>  
                </thead>
                
                <tbody>
                  <tr>
                    <td>
                      <div>
                        <h3>Implementação de Sistemas de Qualidade e Satisfação na Formação  (8h)</h3>
                        <p>Início: 1 de Fevereiro</p>
                        <p>Fim: 20 de Fevereiro</p>
                      </div>
                    </td>

                    <td>
                      <div>
                        <h3>€94,90</h3>
                      </div>
                    </td>
                  </tr>  
                  <tr>
                    <td colspan="2">
                      <div class="total">
                        <h3>Total</h3>
                        <h3>€94,90</h3>
                      </div>
                    </td>
                  </tr>
                </tbody>

              </table>
            </div>

          </article>



          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Dados de Facturação</h2>
              
              <article class="data-box">

                <ul class="dados-fac--confirm">
                  <li>
                    <h2>Maria Luísa dos Santos </h2>
                  </li>
                  <li>
                    <h2>Contribuinte nº 11190387 <span>Portugal</span></h2>
                  </li>
                  <li>
                    <h2>Rua do Alecrim, 29, 2º dto <span>1200-371</span><span>Lisboa</span> </h2>
                  </li>
                </ul>

              </article>

            </div>

          </article>



          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Método de pagamento</h2>
              
              <article class="data-box">

                <ul class="dados-fac--confirm">
                  <li>
                    <h2>Multibanco</h2>
                  </li>  
                </ul>

              </article>

            </div>

          </article> 


          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Descontos</h2>
              
              <article class="data-box">

                <ul class="dados-fac--confirm">
                  <li>
                    <h2>Não possui descontos a aplicar</h2>
                  </li>  
                </ul>

              </article>

            </div>

          </article> 

          
          <!-- MB -->

          <section class="mb-pay--holder cheque-type">
            
            <div class="info-payment">
              <h2 class="h2-pp">Envie o vale à ordem de Cação e Melo, Lda. para::</h2>

              <div class="payment-list">
                <ul>
                  <li>
                    <h1>EVOLUI.COM®</h1>
                  </li>

                  <li>
                    <h1>Ilha da Morraceira, Apartado 3 - Bairro da Estação</h1>
                  </li>

                  <li>
                    <h1>3081-851 Figueira da Foz</h1>
                  </li>
                </ul>
              </div>
            
            </div>

            <div class="pay-holder--box">
                
                <h2>Outros métodos de pagamento</h2>

                <div>
                  <h1 class="h2-pp">Cheque ou Vale Postal</h1>
                  <p>
                    Se pretender pagar numa caixa MultiBanco ou no seu home banking, solicite os dados de pagamento por  <a class="scroll-to" href="#">e-mail</a> ou telefone (+351 233 412 315).
                  </p>
                </div>

                <div>
                  <h1 class="h2-pp">Depósito</h1>
                  <p>
                    Efectue o depósito ou a transferência bancária para a conta <span>127.10.001138-7</span> do <span>Banco Montepio</span>.
                  </p>
                  <p>
                    <span class="bigger-span">NIB da conta: 00.360.127.991.000.113.87.47</span>
                  </p>
                  <p>
                    Caso efectue uma transferência internacional, utilize ainda os seguintes dados:
                  </p>
                  <p>
                    <span class="bigger-span">IBAN: PT50 0036 0127 9910 0011 3874 7 <br> BIC/SWIFT: MPIOPTPL</span>
                  </p>
                  <p>
                    De seguida, envie o comprovativo de depósito ou transferência, <a href="#" class="scroll-to">por email, fax ou carta para os endereços abaixo indicados</a>.
                  </p>
                </div>

                <div>
                  <h1 class="h2-pp">Cartão de Crédito</h1>
                  <p>
                    O pagamento por cartão de crédito é assegurado pela UNICRE. Ao clicar no botão abaixo será reencaminhado para o serviço 
da UNICRE onde deverá introduzir os seus dados de pagamento.
                  </p>
                  <a href="#" class="btn-orange">Comprar com cartão de crédito</a>
                </div>

                <div>
                  <h1 class="h2-pp">Descontos</h1>
                  <p>
                    Caso disponha de vales de desconto ou usufrua de condições especiais, envie-os por <a href="#" class="scroll-to">e-mail, fax ou carta</a>, juntamente 
com o seu comprovativo de pagamento para os <a href="#" class="scroll-to">endereços abaixo indicados</a>.
                  </p>
                </div>


          </section>

          <div class="contacts-holder">
            <h1>Contactos</h1>
            <ul class="contacts-list">
              <li>
                <div class="contacts-list--holder">
                  <span class="icon icon-phone"></span>
                  <hgroup>
                    <h1>Telefone</h1>
                    <h2>+351 233 412 315</h2>
                    <p>(dias úteis das 9h às 18h)</p>
                  </hgroup>
                </div>

                <div class="contacts-list--holder">
                  <span class="icon icon-printer"></span>
                  <hgroup>
                    <h1>Fax</h1>
                    <h2>+351 233 412 317</h2>
                  </hgroup>
                </div>
              </li>

              <li>
                <div class="contacts-list--holder">
                  <span class="icon icon-mailer"></span>
                  <hgroup>
                    <h1>E-mail</h1>
                    <h2>info@evolui.com</h2>
                  </hgroup>
                </div>
                
              </li>

              <li>
                <div class="contacts-list--holder">
                  <span class="icon icon-marker"></span>
                  <hgroup>
                    <h1>Morada</h1>
                    <h2>Ilha da Morraceira</h2>
                    <p>
                      À antiga seca do bacalhau<br>
                      3090-707 Figueira da Foz<br>
                      Portugal
                    </p>
                  </hgroup>
                </div>
                
              </li>
            </ul>
          </div>



        </section> 

    </main>
      </div>

   

    <footer class="footer">
      <?php echo $footer;?>  
    </footer>

  
    </section>
  </body>
</html>







