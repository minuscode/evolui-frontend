<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="catalogo-listagem">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>
      <?php echo $search;?>



      <div class="wrapper">
        <div class="order-catalogue">
        
        <div class="order-catalogue--holder">
          <h1>Faça a sua pesquisa de acordo com os parâmetros abaixo</h1>
          <ul>
            <li><a class="active" href="#">Ordem alfabética</a></li>
            <li><a href="#">Data de início</a></li>
            <li><a href="#">Novos</a></li>
            <li><a href="#">Promoções</a></li>
            <li><a href="#">Especializações</a></li>
            <li><a class="link-catalogo" href="catalogo.php"><span class="icon icon-grid-1"></span></a></li>
            <li><a class="link-cat-listagem" href="catalogo-listagem.php"><span class="icon icon-list-1"></span></a></li>
          </ul>
        </div>
      </div>


      <main class="main">


        <section class="course-holder">

          <ul class="table-course--titles">
            <li>Início</li>
            <li>Nome</li>
            <li>Horas</li>
            <li>Preço</li>
          </ul>

          <a href="detalhe-curso.php" class="table-course--link">
            <ul class="table-course--list">
              <li>18 Jan</li>
              <li>Actividade de Transporte e a Logística</li>
              <li>16</li>
              <li>€74,90</li>
            </ul>
          </a>

          <a href="#" class="table-course--link">
            <ul class="table-course--list">
              <li>2 Fev</li>
              <li>A Massagem ao Bebé (Saúde Infantil)</li>
              <li>8</li>
              <li>€49,90</li>
            </ul>
          </a>


          <a href="#" class="table-course--link">
            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
            <ul class="table-course--list promo-holder">
                <li>1 Jan</li>
                <li>A Pedagogia das Tecnologias da Comunicação na Educação/ Formação</li>
                <li>14</li>
                <li class="promo-holder--items">
                  <span>€79,90</span>
                  <span class="promo-holder--items_price">€63,92</span>
                </li>
            </ul>
          </a>  

          <a href="#" class="table-course--link">
            <ul class="table-course--list">
                <li>29 Jan</li>
                <li>A Relação Escola/Família e o Sucesso Educativo</li>
                <li>6</li>
                <li>€39,90</li>
            </ul>
          </a>

          <a href="#" class="table-course--link">
            <ul class="table-course--list">
                <li>28 Fev</li>
                <li>A Utilização das TIC no pré-escolar</li>
                <li>10</li>
                <li>€69,90</li>
            </ul>
          </li> 

          
          <a href="#" class="table-course--link">
            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
            <ul class="table-course--list promo-holder">
                <li>19 Jan</li>
                <li>Aconselhamento de Gerontologia</li>
                <li>10</li>
                <li class="promo-holder--items">
                  <span>€55,00</span>
                  <span class="promo-holder--items_price">€44,00</span>
                </li>
            </ul>
          </a>  

          <a href="#" class="table-course--link">
            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
            <ul class="table-course--list promo-holder">
                <li>1 Fev</li>
                <li>Actualização Científica e Técnica em Segurança e Higiene do ulabalho</li>
                <li>30</li>
                <li class="promo-holder--items">
                  <span class="promo-holder--items_price">€150,00</span>
                </li>
            </ul>
          </a>
          
          <a href="#" class="table-course--link">
            <ul class="table-course--list">
                <li>23 Jan</li>
                <li>Acção Educativa - Acompanhamento de Crianças</li>
                <li>16</li>
                <li>€69,90</li>
            </ul>
          </a>

          <a href="#" class="table-course--link">
            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
            <ul class="table-course--list promo-holder">
                <li>31 Fev</li>
                <li>Adminisulação de Imóveis</li>
                <li>12</li>
                <li class="promo-holder--items">
                  <span class="promo-holder--items_price">€69,90</span>
                </li>
            </ul>
          </a>


          <a href="#" class="table-course--link">
            <ul class="table-course--list">
                <li>22 Jan</li>
                <li>Adobe Premiere Pro</li>
                <li>16</li>
                <li>€110,00</li>
            </ul>
          </a>

          <a href="#" class="table-course--link">
            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
            <ul class="table-course--list promo-holder">
                <li>26 Jan</li>
                <li>Alemão Comercial</li>
                <li>14</li>
                <li class="promo-holder--items">
                  <span>€59,90</span>
                  <span class="promo-holder--items_price">€47,92</span>
                </li>
            </ul>
          </a>

          <a href="#" class="table-course--link">
            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
            <ul class="table-course--list promo-holder">
                <li>27 Jan</li>
                <li>Alemão I</li>
                <li>14</li>
                <li class="promo-holder--items">
                  <span>€59,90</span>
                  <span class="promo-holder--items_price">€47,92</span>
                </li>
            </ul>
          </a>

          <a href="#" class="table-course--link">
            <ul class="table-course--list">
                <li>2 Fev</li>
                <li>Alemão II</li>
                <li>14</li>
                <li>€59,90</li>
            </ul>
          </a>
          
          <a href="#" class="table-course--link">
            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
            <ul class="table-course--list promo-holder">
                <li>22 Jan</li>
                <li>Alimentação Saudável e Equilibrada</li>
                <li>6</li>
                <li class="promo-holder--items">
                  <span>€59,90</span>
                  <span class="promo-holder--items_price">€47,92</span>
                </li>
            </ul>
          </a>  

<!-- 
          <table class="table-course--list" cellpadding="0" cellspacing="0">
              <ul>
                  <th>Início</th>
                  <th>Nome</th>
                  <th>Horas</th>
                  <th>Preço</th>
              </ul>
              <ul>
                  <li>18 Jan</li>
                  <li>Actividade de ulansporte e a Logística</li>
                  <li>16</li>
                  <li>€74,90</li>
              </ul>
              <ul>
                  <li>2 Fev</li>
                  <li>A Massagem ao Bebé (Saúde Infantil)</li>
                  <li>8</li>
                  <li>€49,90</li>
              </ul>
              <ul class="promo-holder">
                  <li>1 Jan</li>
                  <li>A Pedagogia das Tecnologias da Comunicação na Educação/ Formação</li>
                  <li>14</li>
                  <li class="promo-holder--items">
                    <span>€79,90</span>
                    <span class="promo-holder--items_price">€63,92</span>
                    <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
                  </li>
              </ul>
              <ul>
                  <li>29 Jan</li>
                  <li>A Relação Escola/Família e o Sucesso Educativo</li>
                  <li>6</li>
                  <li>€39,90</li>
              </ul>
              <ul>
                  <li>28 Fev</li>
                  <li>A Utilização das TIC no pré-escolar/li>
                  <li>10</li>
                  <li>€69,90</li>
              </ul>
              <ul class="promo-holder">
                  <li>19 Jan</li>
                  <li>Aconselhamento de Gerontologia</li>
                  <li>10</li>
                  <li class="promo-holder--items">
                    <span>€55,00</span>
                    <span class="promo-holder--items_price">€44,00</span>
                    <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
                  </li>
              </ul>
              <ul class="promo-holder">
                  <li>1 Fev</li>
                  <li>Actualização Científica e Técnica em Segurança e Higiene do ulabalho</li>
                  <li>30</li>
                  <li class="promo-holder--items">
                    <span class="promo-holder--items_price">€150,00</span>
                    <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
                  </li>
              </ul>
              <ul>
                  <li>23 Jan</li>
                  <li>Acção Educativa - Acompanhamento de Crianças</li>
                  <li>16</li>
                  <li>€69,90</li>
              </ul>
              <ul class="promo-holder">
                  <li>31 Fev</li>
                  <li>Adminisulação de Imóveis</li>
                  <li>12</li>
                  <li class="promo-holder--items">
                    <span class="promo-holder--items_price">€69,90</span>
                    <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
                  </li>
              </ul>
              <ul>
                  <li>22 Jan</li>
                  <li>Adobe Premiere Pro</li>
                  <li>16</li>
                  <li>€110,00</li>
              </ul>
              <ul class="promo-holder">
                  <li>26 Jan</li>
                  <li>Alemão Comercial</li>
                  <li>14</li>
                  <li class="promo-holder--items">
                    <span>€59,90</span>
                    <span class="promo-holder--items_price">€47,92</span>
                    <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
                  </li>
              </ul>
              <ul class="promo-holder">
                  <li>27 Jan</li>
                  <li>Alemão I</li>
                  <li>14</li>
                  <li class="promo-holder--items">
                    <span>€59,90</span>
                    <span class="promo-holder--items_price">€47,92</span>
                    <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
                  </li>
              </ul>
              <ul>
                  <li>2 Fev</li>
                  <li>Alemão II</li>
                  <li>14</li>
                  <li>€59,90</li>
              </ul>
              <ul class="promo-holder">
                  <li>22 Jan</li>
                  <li>Alimentação Saudável e Equilibrada</li>
                  <li>6</li>
                  <li class="promo-holder--items">
                    <span>€59,90</span>
                    <span class="promo-holder--items_price">€47,92</span>
                    <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">
                  </li>
              </ul>

          </table>     
 -->


        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







