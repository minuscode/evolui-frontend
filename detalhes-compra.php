<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="pay-page">
      
    
      <!-- HEADER -->

      <header class="header">

        <?php echo $menu;?>  

      </header>
      <?php echo $search;?>


      
      <div class="wrapper">
        <main class="main">


        <section class="pay-details--holder">


          <article class="pay-progress">
            
            <ul class="pay-progress--holder">
              <li class="active">
                <div class="circle">
                  <span>1</span>  
                </div>
                <p class="desc">Detalhes de Compra</p>
              </li>
              <li>
                <div class="circle">
                  <span>2</span>  
                </div>
                <p class="desc">Confirmação</p>
              </li>
              <li>
                <div class="circle">
                  <span>3</span>  
                </div>
                <p class="desc">Resumo</p>
              </li>
            </ul>

          </article>
        
 
          <article class="pay-holder">

            <div class="pay-holder--box">
              <table class="pay-table">
                <thead>
                  <tr>
                    <th>
                      <div>
                        <h2>Descrição</h2>
                      </div>
                    </th>
                    <th>
                      <div>
                        <h2>Preço</h2>
                      </div>
                    </th>
                  </tr>  
                </thead>
                
                <tbody>
                  <tr>
                    <td>
                      <div>
                        <h3>Implementação de Sistemas de Qualidade e Satisfação na Formação  (8h)</h3>
                        <p>Início: 1 de Fevereiro</p>
                        <p>Fim: 20 de Fevereiro</p>
                        <ul>
                          <li><a href="#">Remover curso</a></li>
                          <li><a href="#">Alterar Data</a></li>
                          <li><a href="#">Guardar para mais tarde</a></li>
                        </ul>
                      </div>
                    </td>

                    <td>
                      <div>
                        <h3>€94,90</h3>
                      </div>
                    </td>
                  </tr>  
                  <tr>
                    <td colspan="2">
                      <div class="total">
                        <h3>Total</h3>
                        <h3>€94,90</h3>
                      </div>
                    </td>
                  </tr>
                </tbody>

              </table>
            </div>

          </article>



          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Dados de Facturação</h2>
              
              <article class="data-box">

                <div class="input-box input-100">
                  <div class="input-med">
                    <input type="text" name="nome-fatura" placeholder="Nome da entidade a Facturar" class="input-holder">
                  </div>
                  
                  <div class="input-med">
                    <input type="text" name="num-contribuinte" placeholder="Nº de Contribuinte" class="input-holder">
                  </div>
                </div>

                <div class="input-box input-100">
                  <div class="input-big">
                    <input type="text" name="morada-fatura" placeholder="Morada de Facturação" class="input-holder">
                  </div>
                  
                  <div class="input-big">
                    <div class="input-min">
                      <input type="text" name="cod-postal" placeholder="Código Postal" class="input-holder">
                    </div>
                    <div class="input-med-2">
                      <input type="text" name="localidade" placeholder="Localidade" class="input-holder">
                    </div>
                  </div>
                </div>

                 <div class="input-box input-med">
                  <select name="doc-select" id="doc-select" class="doc-select input-holder">
                    <option value="pt">Portugal</option>
                    <option value="es">Espanha</option>
                    <option value="en">Inglaterra</option>
                  </select>
                </div>  

              </article>

            </div>

          </article>



          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Método de Pagamento</h2>
              
              <article class="data-box">
                <ul class="checkbox-list">
                  <li>
                    <div class="checkbox-holder">
                      <input class="check-box" type="radio" id="multibanco" name="checkbox-terms" value="none">
                      <label for="multibanco" class="checkbox-terms--text">Multibanco</label>
                    </div>

                    <div class="checkbox-holder">
                      <input class="check-box" type="radio" id="transf-deposit" name="checkbox-terms" value="none">
                      <label for="transf-deposit" class="checkbox-terms--text">Depósito ou transferência bancária</label>
                    </div>

                    <div class="checkbox-holder">
                      <input class="check-box" type="radio" id="credit" name="checkbox-terms" value="none">
                      <label for="credit" class="checkbox-terms--text">Cartão de Crédito</label>
                    </div>
                  </li>

                  <li>
                    <div class="checkbox-holder">
                      <input class="check-box" type="radio" id="cheque" name="checkbox-terms" value="none">
                      <label for="cheque" class="checkbox-terms--text">Cheque</label>
                    </div>

                    <div class="checkbox-holder">
                      <input class="check-box" type="radio" id="vale" name="checkbox-terms" value="none">
                      <label for="vale" class="checkbox-terms--text">Vale postal nacional ou internacional</label>
                    </div>
                  </li>
                </ul>
              </article>

            </div>

          </article>   


          <article class="pay-holder">
            
            <div class="pay-holder--box">
              
              <h2>Descontos</h2>
              
              <article class="data-box">
                <ul>
                  <li class="vertical-align--holder">
                    <div class="checkbox-holder vertical-align">
                      <input class="check-box" type="checkbox" id="vale-nacional" name="checkbox-terms" value="none">
                      <label for="vale-nacional" class="checkbox-terms--text">Vale postal nacional ou internacional:</label>
                    </div>

                    <div class="input-box input-big">
                      <input type="text" name="discount" placeholder="Inserir código promocional" class="input-holder input-med">
                      <a class="apply-code" href="#">Aplicar</a>
                    </div>
                  </li>

                  <li>
                    <div class="checkbox-holder vertical-align">
                      <input class="check-box" type="checkbox" id="vale-evolui" name="checkbox-terms" value="none">
                      <label for="vale-evolui" class="checkbox-terms--text">Tenho um desconto associado a parceiros do EVOLUI.COM</label>
                    </div>

                  </li>
                </ul>
              </article>

            </div>

          </article>



          <a class="btn-orange next-step" href="confirmacao-pagamento.php">Continuar</a>   



        </section> 

    </main>
      </div>

   

    <footer class="footer">
      <?php echo $footer;?>  
    </footer>

  
    </section>
  </body>
</html>







