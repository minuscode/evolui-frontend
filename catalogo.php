<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="catalogue-page">
      
    
      <!-- HEADER -->

      <header class="header">
        <?php echo $menu;?>  
      </header>
      <?php echo $search;?>



      <div class="wrapper">
        <div class="order-catalogue">  
        
        <div class="order-catalogue--holder">
          <h1>Faça a sua pesquisa de acordo com os parâmetros abaixo</h1>
          <ul>
            <li><a class="active" href="#">Ordem alfabética</a></li>
            <li><a href="#">Data de início</a></li>
            <li><a href="#">Novos</a></li>
            <li><a href="#">Promoções</a></li>
            <li><a href="#">Especializações</a></li>
            <li><a class="link-catalogo" href="catalogo.php"><span class="icon icon-grid-1"></span></a></li>
            <li><a class="link-cat-listagem" href="catalogo-listagem.php"><span class="icon icon-list-1"></span></a></li>
          </ul>
        </div>
      </div>


      <main class="main">


        <section class="course-holder">

          <article class="course-holder--box">

            <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">

            <img src="assets/img/thumbs/gestao-comercial/transportes-thumb.jpg" alt="A Actividade de Transporte e a Logística">

            <div class="course-title">
              <h1>A Actividade de Transporte e a Logística</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€74,90</p>
                <p class="course-date">18 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  A Actividade de Transporte e a Logística
                </a>
              </div>

            </div>

          </article>

        <!-- 2 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/catalogo/massagem-thumb.jpg" alt="A Massagem ao Bebé (Saúde Infantil)">

            <div class="course-title">
              <h1>A Massagem ao Bebé (Saúde Infantil)</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€49,90</p>
                <p class="course-date">2 de Fevereiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  A Massagem ao Bebé (Saúde Infantil)
                </a>
              </div>

            </div>

          </article>


        <!-- 3 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/catalogo/educacao-thumb.jpg" alt="A Pedagogia das Tecnologias da Comunicação na Educação/Formação">

            <div class="course-title">
              <h1>A Pedagogia das Tecnologias da Comunicação na Educação/Formação</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€63,92</p>
                <p class="course-date">1 de Fevereiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  A Pedagogia das Tecnologias da Comunicação na Educação/Formação
                </a>
              </div>

            </div>

          </article>

        <!-- 4 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/catalogo/escola-thumb.jpg" alt="A Relação Escola/Família e o Sucesso Educativo">

            <div class="course-title">
              <h1>A Relação Escola/Família e o Sucesso Educativo</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€39,90</p>
                <p class="course-date">29 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  A Relação Escola/Família e o Sucesso Educativo
                </a>
              </div>

            </div>

          </article>


        <!-- 5 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/catalogo/tic-thumb.jpg" alt="A Utilização das TIC no Pré-Escolar">

            <div class="course-title">
              <h1>A Utilização das TIC no Pré-Escolar</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€39,90</p>
                <p class="course-date">29 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  A Utilização das TIC no Pré-Escolar
                </a>
              </div>

            </div>

          </article>

        <!-- 6 -->

        <article class="course-holder--box">

          <img class="promo-icon" src="assets/img/icons/promo-icon.png" alt="promo">

          <img src="assets/img/thumbs/catalogo/geron-thumb.jpg" alt="Aconselhamento em Gerontologia">

          <div class="course-title">
            <h1>Aconselhamento em Gerontologia</h1>
          </div>


          <div class="course-price">
            <div class="course-price--holder">
              <p>€44,00</p>
              <p class="course-date">19 de Janeiro</p>
            </div>
            
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Aconselhamento em Gerontologia
              </a>
            </div>

          </div>

        </article>

        <!-- 7 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/catalogo/tecnica-thumb.jpg" alt="Actualização Científica e Técnica em Segurança e Higiene do Trabalho">

          <div class="course-title">
            <h1>Actualização Científica e Técnica em Segurança e Higiene do Trabalho</h1>
          </div>


          <div class="course-price">
            <div class="course-price--holder">
              <p>€150,00</p>
              <p class="course-date">1 de Fevereiro</p>
            </div>
            
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Actualização Científica e Técnica em Segurança e Higiene do Trabalho
              </a>
            </div>

          </div>

        </article>

        <!-- 8 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/catalogo/criancas-thumb.jpg" alt="Acção Educativa - Acompanhamento de Crianças">

          <div class="course-title">
            <h1>Acção Educativa - Acompanhamento de Crianças</h1>
          </div>


          <div class="course-price">
            <div class="course-price--holder">
              <p>€69,90</p>
              <p class="course-date">23 de Janeiro</p>
            </div>
            
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Acção Educativa - Acompanhamento de Crianças
              </a>
            </div>

          </div>

        </article>


        <!-- 9 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/catalogo/admin-thumb.jpg" alt="Administração de Imóveis">

          <div class="course-title">
            <h1>Administração de Imóveis</h1>
          </div>


          <div class="course-price">
            <div class="course-price--holder">
              <p>€69,90</p>
              <p class="course-date">31 de Janeiro</p>
            </div>
            
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Administração de Imóveis
              </a>
            </div>

          </div>

        </article>


        <!-- 10 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/catalogo/adobe-thumb.jpg" alt="Adobe Premiere Pro">

          <div class="course-title">
            <h1>Adobe Premiere Pro</h1>
          </div>


          <div class="course-price">
            <div class="course-price--holder">
              <p>€110,00</p>
              <p class="course-date">31 de Janeiro</p>
            </div>
            
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Adobe Premiere Pro
              </a>
            </div>

          </div>

        </article>

        <!-- 11 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/catalogo/alemao-thumb.jpg" alt="Alemão Comercial">

          <div class="course-title">
            <h1>Alemão Comercial</h1>
          </div>


          <div class="course-price">
            <div class="course-price--holder">
              <p>€47,92</p>
              <p class="course-date">26 de Janeiro</p>
            </div>
            
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Alemão Comercial
              </a>
            </div>

          </div>

        </article>

        <!-- 12 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/catalogo/alemao1-thumb.jpg" alt="Alemão I">

          <div class="course-title">
            <h1>Alemão I</h1>
          </div>


          <div class="course-price">
            <div class="course-price--holder">
              <p>€47,92</p>
              <p class="course-date">27 de Janeiro</p>
            </div>
            
            <a href="#" class="add-favorite">
              <div>
                <span class="icon icon-star"></span>
                <span>Marcar como favorito</span>  
              </div>
              
            </a>

            <div class="about-course">
              <a href="#">
                Alemão I
              </a>
            </div>

          </div>

        </article>


      </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







