<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/libs/jquery.expandable.js"></script>
      <script src="src/js/libs/jquery.tooltipster.min.js"></script>
      <script src="src/js/global.js"></script>


      <script>
 
        $( document ).ready(function() {

          $(".icon-attach").click(function(){
              var $self = $(this), $frm = $self.closest("form"), $input = $frm.find(".input-attachment");
          });         

          $(".input-attachment").change(function(){
              var $self = $(this), $frm = $self.closest("form"), $input = $frm.find(".file-name");
              $input.text($self.val());
              $(".file-holder").fadeIn(300);
          });

          $(".delete-attached-file").click(function(a){
              a.preventDefault();
             $(".file-holder").fadeOut(300);
          });

          $(".attached-files").click(function(a){
              a.preventDefault();
             $(this).parent().find(".file-holder").fadeIn(300);
          });

          

          $('textarea').on('keyup', function(e){
            e.preventDefault();

            if (e.keyCode === 13 && !e.ctrlKey) {
              console.log('clicaste enter');
              $(this).parents('article').find(".btn-publish").click();
            }

          });

          $('textarea').expandable();

          $('.tooltip').tooltipster({
             animation: 'fade',
             delay: 0,
             theme: 'tooltipster-light',
             touchDevices: false,
             trigger: 'hover'
          });
   
          
        });  

      </script>

 

 

  </head>
  <body> 

    <?php include 'include.php';?> 

    <section class="forum-page">
      
    
      <!-- HEADER -->

      <header class="header">
        <?php echo $loged_in;?>
      </header>
      <?php echo $search;?>
      
        

      <div class="wrapper">
        <main class="main">


        <section class="course-details">


          <article class="course-header">
            
            <div class="course-title">
          
              <h1>A Actividade de Transporte e a Logística</h1>

              <ul>
                <li>
                  <span class="icon icon-date"></span>
                  <p>de 21 a 31 de Janeiro de 2015</p>
                </li>
                <li>
                  <span class="icon icon-lessons"></span>
                  <p>5 aulas de 1 em 1 dia</p>
                </li>
                <li>
                  <span class="icon icon-time"></span>
                  <p>16 horas</p>
                </li>
              </ul>

            </div>

            <div class="course-favorite">
              <hgroup>
                <h1>Ana Rita Xavier</h1>
                <h2>Formadora</h2>
              </hgroup>
              <img class="photo-forma" src="assets/img/formadores/ana-rita-xavier.png" alt="Ana Rita Xavier">
            </div>

          </article>
        


          <article class="course-detail--holder">

            <div class="forum-holder--post">

              <h1 class="forum-holder--title">Propôr novo tópico</h1>

              <form  name="new-post" method="POST" action="#">
                <input type="text" name="title" placeholder="Insira o título para o tópico" class="input-holder">
                <textarea type="text" name="subject" placeholder="Assunto" class="input-holder"></textarea>

                <div class="file-holder">
                  <span class="icon icon-photo-1"></span>
                  <a href="#" target="_blank" class="file-name">File name</a>
                  <a href="#" class="icon icon-close-2 delete-attached-file"></a>
                </div>
                <div class="file-holder">
                  <span class="icon icon-doc"></span>
                  <a href="#" target="_blank" class="file-name">File name</a>
                  <a href="#" class="icon icon-close-2 delete-attached-file"></a>
                </div>

                <div class="attachment-holder">

                  <input type="file" name="input-attachment" id="attachment" class="input-attachment">
                  <label for="attachment" class="icon icon-attach"></label>

                </div>

                <input type="submit" value="Publicar" class="btn-orange btn-publish">
              </form>
            </div>

            




            <div class="forum-holder--post remove-padding">

              <img src="assets/img/icons/post-icon-sms.png" alt="Post Icon" class="post-icon">

              <div class="topic-title">
                <img class="copy-letter" src="assets/img/icons/copy-e.png" alt="Copy E">
                <hgroup>
                  <h1 class="forum-holder--title">Final do Curso - Amanhã</h1>
                  <h2>Publicado por EVOLUI.COM <span class="user-acc">administrador</span></h2>
                </hgroup>
              </div>

              <div class="topic-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>

              <div class="published-holder">
                <p>Publicado há 3 horas</p>

                <span class="comments-num">0</span>
                <span class="icon icon-reply"></span>
              </div>

              <form name="new-post" class="new-post" method="POST" action="#">
               <div class="post-content">
                 <img src="assets/img/user-img/user.png" alt="" class="user-img">
                  <div class="post-holder">
                    <textarea type="text" name="comment" placeholder="Escreva um comentário..." class="input-holder" rows="1"></textarea>
                    <div class="attachment-holder">
                      <input type="file" name="attachment" id="attachment" class="input-attachment">
                      <label for="attachment" class="icon icon-attach tooltip" title="Anexar um ou mais documentos"></label>
                    </div>
                  </div>

                 <!--  <input type="submit" value="Publicar" class="btn-orange"> -->
               </div>
              </form>

            </div>


            <div class="forum-holder--post remove-padding">

              <img src="assets/img/icons/post-icon-less.png" alt="Post Icon" class="post-icon">

              <div class="topic-title">
                <img class="user-img" src="assets/img/user-img/user-2.png" alt="Copy E">
                <hgroup>
                  <h1 class="forum-holder--title">Aula 1 - <span>Bem-vindo ao EVOLUI.COM - O seu site de Formação Online</span></h1>
                  <h2>Publicado por Filipa Fernandes<span class="user-acc">formador</span></h2>
                </hgroup>
              </div>

              <div class="topic-content">
                <p>Conheça um pouco mais sobre o nosso site e sobre a forma de frequentar um curso em e-learning. Tire todas as dúvidas sobre como aproveitar ao máximo a sua formação.</p>
              </div>

              <div class="published-holder">
                <p>Publicado há 3 horas</p>
                
                <a href="#" class="see-comments">Ver todos os comentários</a>
                <span class="comments-num">4</span>
                <span class="icon icon-reply"></span>
              </div>



              <article class="comments--holder">
                <div class="posted-cooment">
                  <img class="user-img" src="assets/img/user-img/user-3.png" alt="User">

                  <div class="posted-content">
                    <h1>Joana Pires | <span>Publicado há 20 minutos</span></h1>

                    <p class="expand-text short">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo... inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo tium doloremque laudantium, totam rem aperiam eaque.</p>

                    <a href="#" class="read-more">Ler mais</a>
                  </div>
                </div>

                <div class="posted-cooment">
                  <img class="user-img" src="assets/img/user-img/user-5.png" alt="User">

                  <div class="posted-content">
                    <h1>Filipe Martins | <span>Publicado há 20 minutos</span></h1>

                    <p class="expand-text short">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo... inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo tium doloremque laudantium, totam rem aperiam eaque.</p>

                    <a href="#" class="read-more">Ler mais</a>
                  </div>
                </div>

               
                <div class="posted-cooment more-comments">
                  <img class="user-img" src="assets/img/user-img/user-3.png" alt="User">

                  <div class="posted-content">
                    <h1>Joana Pires | <span>Publicado há 20 minutos</span></h1>

                    <p class="expand-text short">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo... inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo tium doloremque laudantium, totam rem aperiam eaque.</p>

                    <a href="#" class="read-more">Ler mais</a>
                  </div>
                </div>

                <div class="posted-cooment more-comments">
                  <img class="user-img" src="assets/img/user-img/user-5.png" alt="User">

                  <div class="posted-content">
                    <h1>Filipe Martins | <span>Publicado há 20 minutos</span></h1>

                    <p class="expand-text short">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo... inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo tium doloremque laudantium, totam rem aperiam eaque.</p>

                    <a href="#" class="read-more">Ler mais</a>
                  </div>
                </div>
               

              </article>



              <form name="new-post" class="new-post" method="POST" action="#">
               <div class="post-content">
                 <img src="assets/img/user-img/user.png" alt="" class="user-img">
                  <div class="post-holder">
                    <textarea type="text" name="comment" placeholder="Escreva um comentário..." class="input-holder" rows="1"></textarea>
                    <div class="attachment-holder">
                      <input type="file" name="attachment" id="attachment" class="input-attachment">
                      <label for="attachment" class="icon icon-attach tooltip" title="Anexar um ou mais documentos"></label>
                    </div>
                  </div>
                 <!--  <input type="submit" value="Publicar" class="btn-orange"> -->
               </div>
              </form>

            </div>







          <div class="forum-holder--post remove-padding">

              <img src="assets/img/icons/post-icon-less.png" alt="Post Icon" class="post-icon">

              <div class="topic-title">
                <img class="user-img" src="assets/img/user-img/user-6.png" alt="Copy E">
                <hgroup>
                  <h1 class="forum-holder--title">Aula 2 - <span>Contextos de Utilização, Competências, Etapas e Planeamento da Criação de Manuais</span></h1>
                  <h2>Publicado por Jorge Gomes<span class="user-acc">formador</span></h2>
                </hgroup>
              </div>

              <div class="topic-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat consectetur adipi</p>
              </div>

              <div class="published-holder">
                <p>Publicado há 3 dias</p>
                 
                <span class="comments-num">2</span>
                <span class="icon icon-reply"></span>
              </div>



              <article class="comments--holder">
                <div class="posted-cooment">
                  <img class="user-img" src="assets/img/user-img/user-3.png" alt="User">

                  <div class="posted-content">
                    <h1>Joana Pires | <span>Publicado há 20 minutos</span></h1>

                    <p class="expand-text short">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo... inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo tium doloremque laudantium, totam rem aperiam eaque.</p>

                    <a href="#" class="attached-files" title="Ver os ficheiros"><span class="icon icon-attach"></span><span class="file-qt">2</span> anexos</a>

                    <div class="file-holder">
                      <span class="icon icon-photo-1"></span>
                      <a href="#" target="_blank" class="file-name">File name</a>
                      <a href="#" class="icon icon-close-2 delete-attached-file"></a>
                    </div>
                    <div class="file-holder">
                      <span class="icon icon-doc"></span>
                      <a href="#" target="_blank" class="file-name">File name</a>
                      <a href="#" class="icon icon-close-2 delete-attached-file"></a>
                    </div>

                    <a href="#" class="read-more">Ler mais</a>
                  </div>
                </div>

                <div class="posted-cooment">
                  <img class="user-img" src="assets/img/user-img/user-5.png" alt="User">

                  <div class="posted-content">
                    <h1>Filipe Martins | <span>Publicado há 20 minutos</span></h1>

                    <p class="expand-text short">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo... inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo tium doloremque laudantium, totam rem aperiam eaque.</p>

                    <a href="#" class="read-more">Ler mais</a>
                  </div>
                </div>

              </article>



              <form name="new-post" class="new-post" method="POST" action="#">
               <div class="post-content">
                 <img src="assets/img/user-img/user.png" alt="" class="user-img">
                  <div class="post-holder">
                    <textarea type="text" name="comment" placeholder="Escreva um comentário..." class="input-holder" rows="1"></textarea>
                    <div class="attachment-holder">
                      <input type="file" name="attachment" id="attachment" class="input-attachment">
                      <label for="attachment" class="icon icon-attach tooltip" title="Anexar um ou mais documentos"></label>
                    </div>
                  </div>
                 <!--  <input type="submit" value="Publicar" class="btn-orange"> -->
               </div>
              </form>

            </div>


          







            <div class="forum-holder--post remove-padding">

              <img src="assets/img/icons/post-icon-list.png" alt="Post Icon" class="post-icon">

              <div class="topic-title">
                <img class="user-img" src="assets/img/user-img/user-7.png" alt="User 7">
                <hgroup>
                  <h1 class="forum-holder--title">Caso Prático</h1>
                  <h2>Publicado por Maria Henriques<span class="user-acc">formando</span></h2>
                </hgroup>
              </div>

              <div class="topic-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>

              <div class="published-holder">
                <p>Publicado há 1 dia</p>

                <span class="comments-num">0</span>
                <span class="icon icon-reply"></span>
              </div>

              <form name="new-post" class="new-post" method="POST" action="#">
               <div class="post-content">
                 <img src="assets/img/user-img/user.png" alt="" class="user-img">
                  <div class="post-holder">
                    <textarea type="text" name="comment" placeholder="Escreva um comentário..." class="input-holder" rows="1"></textarea>
                    <div class="attachment-holder">
                      <input type="file" name="attachment" id="attachment" class="input-attachment">
                      <label for="attachment" class="icon icon-attach tooltip" title="Anexar um ou mais documentos"></label>
                    </div>
                  </div>
                 <!--  <input type="submit" value="Publicar" class="btn-orange"> -->
               </div>
              </form>

            </div>





          </article>


          <!-- ASIDE -->

          <aside class="course-detail--aside">
            
            <div class="course-detail--aside_box active-aside--box">

              <h2>Notificações <span class="notifications">(1)</span></h2>
              <ul class="clearfix">
                <li>
                  
                  <p>
                    <span class="icon icon-info"></span>
                    Sed ut perspiciatis unde
                    iste natus error sit voluptatem 
                    accusantium doloremque laudan
                    totam rem aperiam, eaque ipsa
                  </p>
               
                </li>
              </ul>
            </div>


            <div class="course-detail--aside_box filters-box">

              <h2>Filtros</h2>
              <ul class="clearfix">


                <li>
                  <a href="#" class="filter-holder active">
                    <span class="icon icon-all"></span>
                    <hgroup>
                      <h1>Todos</h1>
                    </hgroup>
                    <p>12</p>
                  </a>
                </li>

                <li>
                  <a href="#" class="filter-holder">
                    <span class="icon icon-less-2"></span>
                    <hgroup>
                      <h1>Aulas</h1>
                    </hgroup>
                    <p>7</p>
                  </a>
                </li>

                <li>
                  <a href="#" class="filter-holder">
                    <span class="icon icon-all"></span>
                    <hgroup>
                      <h1>Discussão</h1>
                      <h2>Formador</h2>
                      <h2>Formandos</h2>
                    </hgroup>
                    <p>3</p>
                  </a>
                </li>

                <li>
                  <a href="#" class="filter-holder">
                    <span class="icon icon-test"></span>
                    <hgroup>
                      <h1>Testes</h1>
                    </hgroup>
                    <p>2</p>
                  </a>
                </li>


              </ul>
            </div>

            <div class="course-detail--aside_box">

              <h2>Documentos</h2>
              <ul class="clearfix doc-list">
                <li>
                  <a href="#" class="">
                    <span class="icon icon-download"></span>
                    <p>Certificado de frequência</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="icon icon-download"></span>
                    <p>Comprovativo de pagamento</p>
                  </a>
                </li>
              </ul>
            </div>

  
          </aside>

        </section> 

    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>  
      </footer>

  
    </section>
  </body>
</html>







