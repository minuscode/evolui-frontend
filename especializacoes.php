<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>
 

  </head>
  <body> 

  <?php include 'include.php';?>

    <section class="especializacoes-page">
      
    
      <!-- HEADER -->

      <header class="header">
        <?php echo $menu;?>
      </header>
      <?php echo $search;?>
      

     <div class="wrapper">
        <div class="banner">
        

        <div class="banner-title--holder">
          <hgroup>
            <h1>Especializações</h1>
            <h2>
              O EVOLUI.COM preparou especialmente para si 19 cursos de especialização
              em áreas tão diversas como a psicologia, a informática, a pedagogia ou a qualidade
            </h2>
          </hgroup>
        </div>
      </div>

      <main class="main">
  
      <!-- ESPECIALIZAÇÕES -->


        <p class="intro-text">
          Seleccione o curso que pretende e escolha entre frequentar todos os módulos ao mesmo tempo ou diferir alguns no tempo, usufruindo à mesma do preço promocional do curso. No final da formação, receberá um certificado por cada módulo terminado.
        </p>

        <section class="course-holder">

          <article class="course-holder--box">

            <img src="assets/img/thumbs/processos-thumb.jpg" alt="Curso Integrado de Processos Formativos">

            <div class="course-title">
              <h1>Curso Integrado de Processos Formativos</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€345,00</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

          </article>

        <!-- 2 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/geral-thumb.jpg" alt="Curso Geral da Qualidade">

          <div class="course-title">
            <h1>Curso Geral da Qualidade</h1>
          </div>

          <div class="course-price">
              <div class="course-price--holder">
                <p>€180,00</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

        </article>


        <!-- 3 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/shst-thumb.jpg" alt="Curso Geral de Qualidade do Ambiente">

          <div class="course-title">
            <h1>Curso Geral de Qualidade do Ambiente</h1>
          </div>

          <div class="course-price">
              <div class="course-price--holder">
                <p>€249,90</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

        </article>

        <!-- 4 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/estatistica-thumb.jpg" alt="Curso Completo de Estatística e Tratamento de Dados">

          <div class="course-title">
            <h1>Curso Completo de Estatística e Tratamento de Dados</h1>
          </div>

          <div class="course-price">
              <div class="course-price--holder">
                <p>€220,00</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

        </article>

        <!-- 5 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/pedagogia-thumb.jpg" alt="Curso Intensivo de Pedagogia e Comunicação">

          <div class="course-title">
            <h1>Curso Intensivo de Pedagogia e Comunicação</h1>
          </div>

          <div class="course-price">
              <div class="course-price--holder">
                <p>€224,90</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

        </article>

        <!-- 6 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/atendimento-thumb.jpg" alt="Curso Integrado de Secretariado e Atendimento">

          <div class="course-title">
            <h1>Curso Integrado de Secretariado e Atendimento</h1>
          </div>

          <div class="course-price">
              <div class="course-price--holder">
                <p>€249,90</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

        </article>

        <!-- 7 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/video-thumb.jpg" alt="Curso Integrado de Imagem e Vídeo">

          <div class="course-title">
            <h1>Curso Integrado de Imagem e Vídeo</h1>
          </div>

          <div class="course-price">
              <div class="course-price--holder">
                <p>€214,90</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

        </article>

        <!-- 8 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/office-thumb.jpg" alt="Curso Completo de Office">

          <div class="course-title">
            <h1>Curso Completo de Office</h1>
          </div>

          <div class="course-price">
              <div class="course-price--holder">
                <p>€149,90</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

        </article>

        <!-- 9 -->

        <article class="course-holder--box">

          <img src="assets/img/thumbs/microsoft-thumb.jpg" alt="Curso Completo de Microsoft Excel">

          <div class="course-title">
            <h1>Curso Completo de Microsoft Excel</h1>
          </div>

          <div class="course-price">
              <div class="course-price--holder">
                <p>€179,90</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Realizado maioritariamente em 
                  e-learning, este curso, homologado 
                  pelo IEFP, permite a obtenção do 
                  Certificado de Competências 
                  Pedagógicas (CCP) de formador profissional (antigo CAP).
                </a>
              </div>

            </div>

        </article>


      </section> 
    </main>
     </div>

   

      <footer class="footer">
        <?php echo $footer;?>
      </footer>


    </section>
  </body>
</html>







