<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="format-detection" content="telephone=no">
      <!--FACEBOOK META -->
      <meta property="og:title" content="evolui"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://www.evolui.html"/>
      <meta property="og:site_name" content="evolui"/>

      <title>Evolui</title>
      <meta property="og:description" content=" "/>
      <meta name="description" content=" ">
      <meta name="keywords" content=" "/>

      <!-- FAVICONS -->
      

      <!-- CSS -->
      <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="assets/css/style.css">     
      
      <!-- JS -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <script src="src/js/global.js"></script>

 

  </head>
  <body> 

  <?php include 'include.php';?> 



  <section class="pesquisa-page">
    

      <!-- HEADER -->

      <header class="header">
        <?php echo $menu;?>
      </header>
      

      <div class="wrapper">
        <main class="main">

        <hgroup class="search-result">
          <h1>2 resultados para a pesquisa “Imóveis”</h1>
          <p>Não encontrou o curso que procura? Consulte o nosso <a href="catalogo.php">catálogo</a>.</p>
        </hgroup>
        <section class="course-holder">


          <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/transportes-thumb.jpg" alt="A Actividade de Transporte e a Logística">

            <div class="course-title">
              <h1>A Actividade de Transporte e a Logística</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€74,90</p>
                <p class="course-date">18 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  A Actividade de Transporte e a Logística
                </a>
              </div>

            </div>

          </article>

        <!-- 2 -->

        <article class="course-holder--box">

            <img src="assets/img/thumbs/gestao-comercial/administracao-thumb.jpg" alt="Administração de Imóveis">

            <div class="course-title">
              <h1>Administração de Imóveis</h1>
            </div>


            <div class="course-price">
              <div class="course-price--holder">
                <p>€69,90</p>
                <p class="course-date">31 de Janeiro</p>
              </div>
              
              <a href="#" class="add-favorite">
                <div>
                  <span class="icon icon-star"></span>
                  <span>Marcar como favorito</span>  
                </div>
                
              </a>

              <div class="about-course">
                <a href="#">
                  Administração de Imóveis
                </a>
              </div>

            </div>

          </article>


      </section> 
    </main>
      </div>

   

      <footer class="footer">
        <?php echo $footer;?>
      </footer>


     </section>
  </body>
</html>







